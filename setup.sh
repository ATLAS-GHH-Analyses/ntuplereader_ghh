workdir=$(pwd)

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh

#localSetupROOT 5.34.32-HiggsComb-p1-x86_64-slc6-gcc48-opt
localSetupROOT 6.06.02-x86_64-slc6-gcc49-opt

export LD_LIBRARY_PATH=$ROOTSYS/lib:$LD_LIBRARY_PATH
export PATH=$ROOTSYS/bin:$PATH
# export PATH=/afs/cern.ch/sw/lcg/external/doxygen/1.8.2/x86_64-slc6-gcc48-opt/bin:$PATH
export PATH=$workdir:$workdir/scripts:$PATH
export PYTHONPATH=$ROOTSYS/lib:$PYTHONDIR/lib:$PYTHONPATH

# include local lib in library path
export LD_LIBRARY_PATH=`pwd`/lib:$LD_LIBRARY_PATH

