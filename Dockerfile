FROM rootproject/root:6.24.06-centos7
ADD . /analysis/
WORKDIR /analysis/
RUN cd /analysis && \
	make -j8 

# Lines needed to run with reana
USER root

