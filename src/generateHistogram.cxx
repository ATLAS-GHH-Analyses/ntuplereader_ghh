#include "generateHistogram.h"
#include "ReadTruthTree.h"
#include "MVATree.h"  
#include "histSvc.h"

#include <TFile.h>
#include <TH1.h>
#include <TKey.h>
#include <TList.h>
#include <TClass.h>
#include <TRegexp.h>
#include "TChain.h"
#include "TLorentzVector.h"

#include <iostream>
#include <fstream>
#include <vector>
#include "TRandom3.h"
#include <dirent.h>

generateHistogram::generateHistogram():
  m_nEvents(-1),
  m_DSID(""),
  m_Debug(false),
  m_Lumi(1000),
  m_SumWeights(0),
  m_XSec(0),
  //Yanhui
  m_MJ(false),
  m_Vgamma(false),
  m_NonPrompt(false),
  m_CBA(false),
  m_MC16a(false),
  m_UseSFs(false),
  m_TightIso(false),
  m_inputChain(NULL),
  m_outputPath("./"),
  m_inputPath("./"),
  hTruthMap_b(NULL),
  hTruthMap_c(NULL),
  hTruthMap_l(NULL),
  hTruthMap_t(NULL)
{

}

generateHistogram::~generateHistogram() {

}

void generateHistogram::SetInputPath(TString path){

  m_inputPath = path;

}

TString generateHistogram::GetOutputPath(){

  return m_outputPath;

}

void generateHistogram::SetOutputPath(TString path){

  m_outputPath = path;

  std::cout << "m_outputPath " << m_outputPath << std::endl;

}

void generateHistogram::SetNEvents(int events){

  m_nEvents = events;

}

void generateHistogram::SetDebug(bool debug){

  m_Debug = debug;

}

void generateHistogram::SetLumi(float lumi){

  m_Lumi = lumi * 1000;  //Convert fb-1 to pb-1
}

void generateHistogram::runSideBand(bool run_sideband){

    m_runSideBand = run_sideband;
}
void generateHistogram::runssWW(bool run_ssWW){

    m_runssWW = run_ssWW;
}
void generateHistogram::RunMJ(bool MJ){

   m_MJ = MJ;
}
void generateHistogram::RunVgamma(bool Vgamma){

    m_Vgamma = Vgamma;
}
void generateHistogram::RunNonPrompt(bool NonP){

  m_NonPrompt = NonP;
}
void generateHistogram::RunCBA(bool CBA){

   m_CBA = CBA;
}
void generateHistogram::RunMC16a(bool MC16a){

   m_MC16a = MC16a;
}
void generateHistogram::UseSFs(bool UseSFs){

   m_UseSFs = UseSFs;
}
void generateHistogram::TightIso(bool TightIso){

  m_TightIso = TightIso;
}
void generateHistogram::GenerateHistogram( AnalysisType type, TString DSID, TString sysTree){
//void generateHistogram::GenerateHistogram( AnalysisType type, Process process, Variation variation){

  StoreConfig(type, DSID);
  FileLoader(DSID, sysTree);
  std::cout << m_inputChain->GetEntries() << std::endl;
  //m_tree = new MVATree(type);//Yanhui
  //std::cout<<"here1"<<"\n";
  m_histFill = new histSvc(m_outputPath);
  //std::cout<<"here2"<<"\n";
  m_histFill->SetAnalysisType(type);
  std::cout<<"here3"<<"\n";
  m_histFill->SetProcess(DSID, m_process);
  //std::cout<<"here4"<<"\n";
  m_histFill->SetSystematicTree(sysTree);
  //std::cout<<"here4"<<"\n";
  readNtuple = new ReadTruthTree(m_inputChain);
  //std::cout<<"here5"<<"\n";

  FillHistograms(DSID);//Here is the loop
  //std::cout<<"here6"<<"\n";
  m_histFill->WriteHists();

  delete m_tree;
  delete m_histFill;
  delete readNtuple;
  delete m_inputChain;

}


void generateHistogram::StoreConfig( AnalysisType type, TString DSID ){

  m_type = type;
  m_DSID = DSID;
  m_process = GetProcessFromDSID(DSID);
//  m_variation = variation;
//  std::cout << m_region.size() << std::endl;
//  std::cout << m_variable.size() << std::endl;


}

generateHistogram::Process generateHistogram::GetProcessFromDSID( TString DSID ){

  std::cout << "DSID " << DSID << std::endl;
  if(m_NonPrompt){ 
    std::cout << "NonPrompt"<< std::endl;
    return generateHistogram::nonprompt;
    }
  else if (m_Vgamma){
    std::cout << "Vgamma"<< std::endl;
	return generateHistogram::photonconversion;
  }
  else {
  if(DSID.Contains("GHH300X")) return generateHistogram::GHH300X;
  if(DSID.Contains("GHH300Y")) return generateHistogram::GHH300Y;
  if(DSID.Contains("GHH600X")) return generateHistogram::GHH600X;
  if(DSID.Contains("GHH600Y")) return generateHistogram::GHH600Y;
  if(DSID.Contains("GHH900X")) return generateHistogram::GHH900X;
  if( DSID.Contains("Wjets") ) return generateHistogram::Wjets;
  if( DSID.Contains("Zjets")) return generateHistogram::Zjets;
  if( DSID.Contains("ttbar") ) return generateHistogram::ttbar;
  if( DSID.Contains("singletops") || DSID.Contains("singletopt") || DSID.Contains("singletopWt") ) return generateHistogram::stop;
  if( DSID.Contains("ssWW")) return generateHistogram::ssWW;
  if( DSID.Contains("WZ")) return generateHistogram::WZ;
  if( DSID.Contains("ZZ")) return generateHistogram::ZZ;
  //if( DSID.Contains("Wgamma")) return generateHistogram::Wgamma;
  //if( DSID.Contains("Zgamma")) return generateHistogram::Zgamma;
  if( DSID.Contains("Zgamma") || DSID.Contains("Wgamma")) return generateHistogram::Vgamma;
  if( DSID.Contains("WWW")) return generateHistogram::WWW;
  if( DSID.Contains("VVV")) return generateHistogram::VVV;
  //if( DSID.Contains("ttW")) return generateHistogram::ttW;
  //if( DSID.Contains("ttZ")) return generateHistogram::ttZ;
  //if( DSID.Contains("tZ")) return generateHistogram::tZ;
  if( DSID.Contains("tz") || DSID.Contains("ttZ") || DSID.Contains("ttW")) return generateHistogram::topX;
  if( DSID.Contains("data")) return generateHistogram::data;
  if( DSID.Contains("GHH3f600f0")){ return generateHistogram::GHH3f600f0;}
  if( DSID.Contains("GHH3fm600f0")){ return generateHistogram::GHH3fm600f0;}
  if( DSID.Contains("GHH3f350f2100")){ return generateHistogram::GHH3f350f2100;}
  if( DSID.Contains("GHH3fm350f2100")){ return generateHistogram::GHH3fm350f2100;}
  if( DSID.Contains("GHH3fm350fm2100")){ return generateHistogram::GHH3fm350fm2100;}
  if( DSID.Contains("GHH3f350fm2100")){ return generateHistogram::GHH3f350fm2100;}
  if( DSID.Contains("GHH3f0f3000")){ return generateHistogram::GHH3f0f3000;}
  if( DSID.Contains("GHH3f0fm3000")){ return generateHistogram::GHH3f0fm3000;}
  if( DSID.Contains("GHH6f650f0")){ return generateHistogram::GHH6f650f0;}
  if( DSID.Contains("GHH6fm650f0")){ return generateHistogram::GHH6fm650f0;}
  if( DSID.Contains("GHH6f400f2400")){ return generateHistogram::GHH6f400f2400;}
  if( DSID.Contains("GHH6fm400f2400")){ return generateHistogram::GHH6fm400f2400;}
  if( DSID.Contains("GHH6fm400fm2400")){ return generateHistogram::GHH6fm400fm2400;}
  if( DSID.Contains("GHH6f400fm2400")){ return generateHistogram::GHH6f400fm2400;}
  if( DSID.Contains("GHH6f0f3500")){ return generateHistogram::GHH6f0f3500;}
  if( DSID.Contains("GHH6f0fm3500")){ return generateHistogram::GHH6f0fm3500;}
  if( DSID.Contains("GHH9f800f0")){ return generateHistogram::GHH9f800f0;}
  if( DSID.Contains("GHH9fm800f0")){ return generateHistogram::GHH9fm800f0;}
  if( DSID.Contains("GHH9f600f3600")){ return generateHistogram::GHH9f600f3600;}
  if( DSID.Contains("GHH9fm600f3600")){ return generateHistogram::GHH9fm600f3600;}
  if( DSID.Contains("GHH9fm600fm3600")){ return generateHistogram::GHH9fm600fm3600;}
  if( DSID.Contains("GHH9f600fm3600")){ return generateHistogram::GHH9f600fm3600;}
  if( DSID.Contains("GHH9f0f5000")){ return generateHistogram::GHH9f0f5000;}
  if( DSID.Contains("GHH9f0fm5000")){ return generateHistogram::GHH9f0fm5000;}
  if( DSID.Contains("GHH300fW1230fWW0")){ return generateHistogram::GHH300fW1230fWW0;} //new samples
  if( DSID.Contains("GHH300fW770fWW0")){ return generateHistogram::GHH300fW770fWW0;}
  if( DSID.Contains("GHH300fW1910fWW0")){ return generateHistogram::GHH300fW1910fWW0;}
  if( DSID.Contains("GHH300fW660fWW3960")){ return generateHistogram::GHH300fW660fWW3960;}
  if( DSID.Contains("GHH300fW415fWW2490")){ return generateHistogram::GHH300fW415fWW2490;}
  if( DSID.Contains("GHH300fW1015fWW6090")){ return generateHistogram::GHH300fW1015fWW6090;}
  if( DSID.Contains("GHH300fW0fWW4600")){ return generateHistogram::GHH300fW0fWW4600;}
  if( DSID.Contains("GHH300fW0fWW2450")){ return generateHistogram::GHH300fW0fWW2450;}
  if( DSID.Contains("GHH300fW0fWW7100")){ return generateHistogram::GHH300fW0fWW7100;}
  if( DSID.Contains("GHH300fWm730fWW4380")){ return generateHistogram::GHH300fWm730fWW4380;}
  if( DSID.Contains("GHH300fWm455fWW2730")){ return generateHistogram::GHH300fWm455fWW2730;}
  if( DSID.Contains("GHH300fWm1125fWW6750")){ return generateHistogram::GHH300fWm1125fWW6750;}
  if( DSID.Contains("GHH300fWm1360fWW0")){ return generateHistogram::GHH300fWm1360fWW0;}
  if( DSID.Contains("GHH300fWm810fWW0")){ return generateHistogram::GHH300fWm810fWW0;}
  if( DSID.Contains("GHH300fWm2180fWW0")){ return generateHistogram::GHH300fWm2180fWW0;}
  if( DSID.Contains("GHH300fWm610fWWm3660")){ return generateHistogram::GHH300fWm610fWWm3660;}
  if( DSID.Contains("GHH300fWm370fWWm2220")){ return generateHistogram::GHH300fWm370fWWm2220;}
  if( DSID.Contains("GHH300fWm960fWWm5760")){ return generateHistogram::GHH300fWm960fWWm5760;}
  if( DSID.Contains("GHH300fW0fWWm4650")){ return generateHistogram::GHH300fW0fWWm4650;}
  if( DSID.Contains("GHH300fW0fWWm2300")){ return generateHistogram::GHH300fW0fWWm2300;}
  if( DSID.Contains("GHH300fW0fWWm7250")){ return generateHistogram::GHH300fW0fWWm7250;}
  if( DSID.Contains("GHH300fW710fWWm4260")){ return generateHistogram::GHH300fW710fWWm4260;}
  if( DSID.Contains("GHH300fW430fWWm2580")){ return generateHistogram::GHH300fW430fWWm2580;}
  if( DSID.Contains("GHH300fW1110fWWm6660")){ return generateHistogram::GHH300fW1110fWWm6660;}
  if( DSID.Contains("GHH300fW1000fWW2000")){ return generateHistogram::GHH300fW1000fWW2000;}
  if( DSID.Contains("GHH300fW500fWW1000")){ return generateHistogram::GHH300fW500fWW1000;}
  if( DSID.Contains("GHH300fW1500fWW3000")){ return generateHistogram::GHH300fW1500fWW3000;}
  if( DSID.Contains("GHH300fW300fWW4500")){ return generateHistogram::GHH300fW300fWW4500;}
  if( DSID.Contains("GHH300fW150fWW2250")){ return generateHistogram::GHH300fW150fWW2250;}
  if( DSID.Contains("GHH300fW470fWW7050")){ return generateHistogram::GHH300fW470fWW7050;}
  if( DSID.Contains("GHH300fWm330fWW4950")){ return generateHistogram::GHH300fWm330fWW4950;}
  if( DSID.Contains("GHH300fWm150fWW2250")){ return generateHistogram::GHH300fWm150fWW2250;}
  if( DSID.Contains("GHH300fWm490fWW7350")){ return generateHistogram::GHH300fWm490fWW7350;}
  if( DSID.Contains("GHH300fWm1200fWW2400")){ return generateHistogram::GHH300fWm1200fWW2400;}
  if( DSID.Contains("GHH300fWm500fWW1000")){ return generateHistogram::GHH300fWm500fWW1000;}
  if( DSID.Contains("GHH300fWm1950fWW3900")){ return generateHistogram::GHH300fWm1950fWW3900;}
  if( DSID.Contains("GHH300fWm1100fWWm2200")){ return generateHistogram::GHH300fWm1100fWWm2200;}
  if( DSID.Contains("GHH300fWm500fWWm1000")){ return generateHistogram::GHH300fWm500fWWm1000;}
  if( DSID.Contains("GHH300fWm1800fWWm3600")){ return generateHistogram::GHH300fWm1800fWWm3600;}
  if( DSID.Contains("GHH300fWm300fWWm4500")){ return generateHistogram::GHH300fWm300fWWm4500;}
  if( DSID.Contains("GHH300fWm140fWWm2100")){ return generateHistogram::GHH300fWm140fWWm2100;}
  if( DSID.Contains("GHH300fWm470fWWm7050")){ return generateHistogram::GHH300fWm470fWWm7050;}
  if( DSID.Contains("GHH300fW330fWWm4950")){ return generateHistogram::GHH300fW330fWWm4950;}
  if( DSID.Contains("GHH300fW150fWWm2250")){ return generateHistogram::GHH300fW150fWWm2250;}
  if( DSID.Contains("GHH300fW500fWWm7500")){ return generateHistogram::GHH300fW500fWWm7500;}
  if( DSID.Contains("GHH300fW1100fWWm2200")){ return generateHistogram::GHH300fW1100fWWm2200;}
  if( DSID.Contains("GHH300fW500fWWm1000")){ return generateHistogram::GHH300fW500fWWm1000;}
  if( DSID.Contains("GHH300fW1800fWWm3600")){ return generateHistogram::GHH300fW1800fWWm3600;}
  if( DSID.Contains("GHH600fW1350fWW0")){ return generateHistogram::GHH600fW1350fWW0;}
  if( DSID.Contains("GHH600fW820fWW0")){ return generateHistogram::GHH600fW820fWW0;}
  if( DSID.Contains("GHH600fW2130fWW0")){ return generateHistogram::GHH600fW2130fWW0;}
  if( DSID.Contains("GHH600fW765fWW4590")){ return generateHistogram::GHH600fW765fWW4590;}
  if( DSID.Contains("GHH600fW470fWW2820")){ return generateHistogram::GHH600fW470fWW2820;}
  if( DSID.Contains("GHH600fW1205fWW7230")){ return generateHistogram::GHH600fW1205fWW7230;}
  if( DSID.Contains("GHH600fW0fWW6200")){ return generateHistogram::GHH600fW0fWW6200;}
  if( DSID.Contains("GHH600fW0fWW3800")){ return generateHistogram::GHH600fW0fWW3800;}
  if( DSID.Contains("GHH600fW0fWW9750")){ return generateHistogram::GHH600fW0fWW9750;}
  if( DSID.Contains("GHH600fWm855fWW5130")){ return generateHistogram::GHH600fWm855fWW5130;}
  if( DSID.Contains("GHH600fWm520fWW3120")){ return generateHistogram::GHH600fWm520fWW3120;}
  if( DSID.Contains("GHH600fWm1355fWW8130")){ return generateHistogram::GHH600fWm1355fWW8130;}
  if( DSID.Contains("GHH600fWm1340fWW0")){ return generateHistogram::GHH600fWm1340fWW0;}
  if( DSID.Contains("GHH600fWm790fWW0")){ return generateHistogram::GHH600fWm790fWW0;}
  if( DSID.Contains("GHH600fWm2130fWW0")){ return generateHistogram::GHH600fWm2130fWW0;}
  if( DSID.Contains("GHH600fWm755fWWm4530")){ return generateHistogram::GHH600fWm755fWWm4530;}
  if( DSID.Contains("GHH600fWm455fWWm2730")){ return generateHistogram::GHH600fWm455fWWm2730;}
  if( DSID.Contains("GHH600fWm1195fWWm7170")){ return generateHistogram::GHH600fWm1195fWWm7170;}
  if( DSID.Contains("GHH600fW0fWWm6250")){ return generateHistogram::GHH600fW0fWWm6250;}
  if( DSID.Contains("GHH600fW0fWWm3800")){ return generateHistogram::GHH600fW0fWWm3800;}
  if( DSID.Contains("GHH600fW0fWWm9900")){ return generateHistogram::GHH600fW0fWWm9900;}
  if( DSID.Contains("GHH600fW915fWWm5490")){ return generateHistogram::GHH600fW915fWWm5490;}
  if( DSID.Contains("GHH600fW555fWWm3330")){ return generateHistogram::GHH600fW555fWWm3330;}
  if( DSID.Contains("GHH600fW1440fWWm8640")){ return generateHistogram::GHH600fW1440fWWm8640;}
  if( DSID.Contains("GHH600fW1200fWW2400")){ return generateHistogram::GHH600fW1200fWW2400;}
  if( DSID.Contains("GHH600fW500fWW1000")){ return generateHistogram::GHH600fW500fWW1000;}
  if( DSID.Contains("GHH600fW1900fWW3800")){ return generateHistogram::GHH600fW1900fWW3800;}
  if( DSID.Contains("GHH600fW400fWW6000")){ return generateHistogram::GHH600fW400fWW6000;}
  if( DSID.Contains("GHH600fW200fWW3000")){ return generateHistogram::GHH600fW200fWW3000;}
  if( DSID.Contains("GHH600fW650fWW9750")){ return generateHistogram::GHH600fW650fWW9750;}
  if( DSID.Contains("GHH600fWm400fWW6000")){ return generateHistogram::GHH600fWm400fWW6000;}
  if( DSID.Contains("GHH600fWm200fWW3000")){ return generateHistogram::GHH600fWm200fWW3000;}
  if( DSID.Contains("GHH600fWm650fWW9750")){ return generateHistogram::GHH600fWm650fWW9750;}
  if( DSID.Contains("GHH600fWm1250fWW2500")){ return generateHistogram::GHH600fWm1250fWW2500;}
  if( DSID.Contains("GHH600fWm500fWW1000")){ return generateHistogram::GHH600fWm500fWW1000;}
  if( DSID.Contains("GHH600fWm2000fWW4000")){ return generateHistogram::GHH600fWm2000fWW4000;}
  if( DSID.Contains("GHH600fWm1200fWWm2400")){ return generateHistogram::GHH600fWm1200fWWm2400;}
  if( DSID.Contains("GHH600fWm500fWWm1000")){ return generateHistogram::GHH600fWm500fWWm1000;}
  if( DSID.Contains("GHH600fWm1900fWWm3800")){ return generateHistogram::GHH600fWm1900fWWm3800;}
  if( DSID.Contains("GHH600fWm400fWWm6000")){ return generateHistogram::GHH600fWm400fWWm6000;}
  if( DSID.Contains("GHH600fWm190fWWm2850")){ return generateHistogram::GHH600fWm190fWWm2850;}
  if( DSID.Contains("GHH600fWm650fWWm9750")){ return generateHistogram::GHH600fWm650fWWm9750;}
  if( DSID.Contains("GHH600fW450fWWm6750")){ return generateHistogram::GHH600fW450fWWm6750;}
  if( DSID.Contains("GHH600fW200fWWm3000")){ return generateHistogram::GHH600fW200fWWm3000;}
  if( DSID.Contains("GHH600fW670fWWm10050")){ return generateHistogram::GHH600fW670fWWm10050;}
  if( DSID.Contains("GHH600fW1300fWWm2600")){ return generateHistogram::GHH600fW1300fWWm2600;}
  if( DSID.Contains("GHH600fW600fWWm1200")){ return generateHistogram::GHH600fW600fWWm1200;}
  if( DSID.Contains("GHH600fW2050fWWm4100")){ return generateHistogram::GHH600fW2050fWWm4100;}
  if( DSID.Contains("GHH900fW1570fWW0")){ return generateHistogram::GHH900fW1570fWW0;}
  if( DSID.Contains("GHH900fW940fWW0")){ return generateHistogram::GHH900fW940fWW0;}
  if( DSID.Contains("GHH900fW2510fWW0")){ return generateHistogram::GHH900fW2510fWW0;}
  if( DSID.Contains("GHH900fW1015fWW6090")){ return generateHistogram::GHH900fW1015fWW6090;}
  if( DSID.Contains("GHH900fW610fWW3660")){ return generateHistogram::GHH900fW610fWW3660;}
  if( DSID.Contains("GHH900fW1615fWW9690")){ return generateHistogram::GHH900fW1615fWW9690;}
  if( DSID.Contains("GHH900fW0fWW8800")){ return generateHistogram::GHH900fW0fWW8800;}
  if( DSID.Contains("GHH900fW0fWW5250")){ return generateHistogram::GHH900fW0fWW5250;}
  if( DSID.Contains("GHH900fW0fWW14100")){ return generateHistogram::GHH900fW0fWW14100;}
  if( DSID.Contains("GHH900fWm1165fWW6990")){ return generateHistogram::GHH900fWm1165fWW6990;}
  if( DSID.Contains("GHH900fWm695fWW4170")){ return generateHistogram::GHH900fWm695fWW4170;}
  if( DSID.Contains("GHH900fWm1865fWW11190")){ return generateHistogram::GHH900fWm1865fWW11190;}
  if( DSID.Contains("GHH900fWm1550fWW0")){ return generateHistogram::GHH900fWm1550fWW0;}
  if( DSID.Contains("GHH900fWm920fWW0")){ return generateHistogram::GHH900fWm920fWW0;}
  if( DSID.Contains("GHH900fWm2480fWW0")){ return generateHistogram::GHH900fWm2480fWW0;}
  if( DSID.Contains("GHH900fWm1000fWWm6000")){ return generateHistogram::GHH900fWm1000fWWm6000;}
  if( DSID.Contains("GHH900fWm495fWWm2970")){ return generateHistogram::GHH900fWm495fWWm2970;}
  if( DSID.Contains("GHH900fWm1605fWWm9630")){ return generateHistogram::GHH900fWm1605fWWm9630;}
  if( DSID.Contains("GHH900fW0fWWm8700")){ return generateHistogram::GHH900fW0fWWm8700;}
  if( DSID.Contains("GHH900fW0fWWm5150")){ return generateHistogram::GHH900fW0fWWm5150;}
  if( DSID.Contains("GHH900fW0fWWm13950")){ return generateHistogram::GHH900fW0fWWm13950;}
  if( DSID.Contains("GHH900fW1205fWWm7230")){ return generateHistogram::GHH900fW1205fWWm7230;}
  if( DSID.Contains("GHH900fW720fWWm4320")){ return generateHistogram::GHH900fW720fWWm4320;}
  if( DSID.Contains("GHH900fW1925fWWm11550")){ return generateHistogram::GHH900fW1925fWWm11550;}
  if( DSID.Contains("GHH900fW1400fWW2800")){ return generateHistogram::GHH900fW1400fWW2800;}
  if( DSID.Contains("GHH900fW700fWW1400")){ return generateHistogram::GHH900fW700fWW1400;}
  if( DSID.Contains("GHH900fW2300fWW4600")){ return generateHistogram::GHH900fW2300fWW4600;}
  if( DSID.Contains("GHH900fW550fWW8250")){ return generateHistogram::GHH900fW550fWW8250;}
  if( DSID.Contains("GHH900fW300fWW4500")){ return generateHistogram::GHH900fW300fWW4500;}
  if( DSID.Contains("GHH900fW900fWW13500")){ return generateHistogram::GHH900fW900fWW13500;}
  if( DSID.Contains("GHH900fWm600fWW9000")){ return generateHistogram::GHH900fWm600fWW9000;}
  if( DSID.Contains("GHH900fWm300fWW4500")){ return generateHistogram::GHH900fWm300fWW4500;}
  if( DSID.Contains("GHH900fWm1000fWW15000")){ return generateHistogram::GHH900fWm1000fWW15000;}
  if( DSID.Contains("GHH900fWm1500fWW3000")){ return generateHistogram::GHH900fWm1500fWW3000;}
  if( DSID.Contains("GHH900fWm800fWW1600")){ return generateHistogram::GHH900fWm800fWW1600;}
  if( DSID.Contains("GHH900fWm2400fWW4800")){ return generateHistogram::GHH900fWm2400fWW4800;}
  if( DSID.Contains("GHH900fWm1400fWWm2800")){ return generateHistogram::GHH900fWm1400fWWm2800;}
  if( DSID.Contains("GHH900fWm700fWWm1400")){ return generateHistogram::GHH900fWm700fWWm1400;}
  if( DSID.Contains("GHH900fWm2300fWWm4600")){ return generateHistogram::GHH900fWm2300fWWm4600;}
  if( DSID.Contains("GHH900fWm550fWWm8250")){ return generateHistogram::GHH900fWm550fWWm8250;}
  if( DSID.Contains("GHH900fWm250fWWm3750")){ return generateHistogram::GHH900fWm250fWWm3750;}
  if( DSID.Contains("GHH900fWm850fWWm12750")){ return generateHistogram::GHH900fWm850fWWm12750;}
  if( DSID.Contains("GHH900fW600fWWm9000")){ return generateHistogram::GHH900fW600fWWm9000;}
  if( DSID.Contains("GHH900fW300fWWm4500")){ return generateHistogram::GHH900fW300fWWm4500;}
  if( DSID.Contains("GHH900fW1000fWWm15000")){ return generateHistogram::GHH900fW1000fWWm15000;}
  if( DSID.Contains("GHH900fW1500fWWm3000")){ return generateHistogram::GHH900fW1500fWWm3000;}
  if( DSID.Contains("GHH900fW800fWWm1600")){ return generateHistogram::GHH900fW800fWWm1600;}
  if( DSID.Contains("GHH900fW2400fWWm4800")){ return generateHistogram::GHH900fW2400fWWm4800;}
  if( DSID.Contains("GHH300fW1350fWW0")){ return generateHistogram::GHH300fW1350fWW0;}
  if( DSID.Contains("GHH360fW1350fWW0")){ return generateHistogram::GHH360fW1350fWW0;}
  if( DSID.Contains("GHH420fW1350fWW0")){ return generateHistogram::GHH420fW1350fWW0;}
  if( DSID.Contains("GHH480fW1350fWW0")){ return generateHistogram::GHH480fW1350fWW0;}
  if( DSID.Contains("GHH540fW1350fWW0")){ return generateHistogram::GHH540fW1350fWW0;}
  if( DSID.Contains("GHH660fW1350fWW0")){ return generateHistogram::GHH660fW1350fWW0;}
  if( DSID.Contains("GHH720fW1350fWW0")){ return generateHistogram::GHH720fW1350fWW0;}
  if( DSID.Contains("GHH780fW1350fWW0")){ return generateHistogram::GHH780fW1350fWW0;}
  if( DSID.Contains("GHH840fW1350fWW0")){ return generateHistogram::GHH840fW1350fWW0;}
  if( DSID.Contains("GHH900fW1350fWW0")){ return generateHistogram::GHH900fW1350fWW0;}
  if( DSID.Contains("GHH300fW0fWW6200")){ return generateHistogram::GHH300fW0fWW6200;}
  if( DSID.Contains("GHH360fW0fWW6200")){ return generateHistogram::GHH360fW0fWW6200;}
  if( DSID.Contains("GHH420fW0fWW6200")){ return generateHistogram::GHH420fW0fWW6200;}
  if( DSID.Contains("GHH480fW0fWW6200")){ return generateHistogram::GHH480fW0fWW6200;}
  if( DSID.Contains("GHH540fW0fWW6200")){ return generateHistogram::GHH540fW0fWW6200;}
  if( DSID.Contains("GHH660fW0fWW6200")){ return generateHistogram::GHH660fW0fWW6200;}
  if( DSID.Contains("GHH720fW0fWW6200")){ return generateHistogram::GHH720fW0fWW6200;}
  if( DSID.Contains("GHH780fW0fWW6200")){ return generateHistogram::GHH780fW0fWW6200;}
  if( DSID.Contains("GHH840fW0fWW6200")){ return generateHistogram::GHH840fW0fWW6200;}
  if( DSID.Contains("GHH900fW0fWW6200")){ return generateHistogram::GHH900fW0fWW6200;}
  if( DSID.Contains("GHH1000fW1350fWW0")){ return generateHistogram::GHH1000fW1350fWW0;}
  if( DSID.Contains("GHH1200fW1350fWW0")){ return generateHistogram::GHH1200fW1350fWW0;}
  if( DSID.Contains("GHH1500fW1350fWW0")){ return generateHistogram::GHH1500fW1350fWW0;}
  if( DSID.Contains("GHH2000fW1350fWW0")){ return generateHistogram::GHH2000fW1350fWW0;}
  if( DSID.Contains("GHH1000fW0fWW6200")){ return generateHistogram::GHH1000fW0fWW6200;}
  if( DSID.Contains("GHH1200fW0fWW6200")){ return generateHistogram::GHH1200fW0fWW6200;}
  if( DSID.Contains("GHH1500fW0fWW6200")){ return generateHistogram::GHH1500fW0fWW6200;}
  if( DSID.Contains("GHH2000fW0fWW6200")){ return generateHistogram::GHH2000fW0fWW6200;}


  else {
    std::cout << "Did not find sample - exiting" << std::endl;
    exit(0);
  }

  }
}


void generateHistogram::FileLoader(TString DSID, TString sysTree ){
  
//TODO Add file loading based on process (i.e. if Whf, load all W+jets samples)
  
  TString ChainName = sysTree; //Nominal;
  //ChainName += DSID;
  std::cout << "sysTree " << sysTree << std::endl;
  m_inputChain = new TChain(ChainName);

  TH1F* h_SumW;
  TH1F* h_XSec;

  TString SumWName = "h_Keep_EventCount_";
  //SumWName += DSID;
  TString XSecName = "h_Keep_CrossSection_";
  //XSecName += DSID;

  TString DSIDStr = "";
  DSIDStr += DSID;
  DSIDStr += ".root"; // Yue
  m_SumWeights = 0;
  m_XSec = 0;

  if( m_inputPath.Contains(".root") ){
    m_inputChain->Add(m_inputPath);
    TFile* loadFile = TFile::Open(m_inputPath);
    h_SumW = (TH1F*) loadFile->Get(SumWName);
    h_XSec = (TH1F*) loadFile->Get(XSecName);
    m_SumWeights = h_SumW->GetBinContent(1);
    m_XSec = h_XSec->GetBinContent(1) / h_XSec->GetEntries();  //Need to divide by entries due to grid merging
  }
  else {
    std::cout << "Input is a directory: going fancy " << std::endl;
	///eos/atlas/user/y/yama/HtRatio/Ntuples/Reduce /*root
	DIR*     dir;
    dirent*  pdir;
    dir = opendir( m_inputPath );     // open current directory
	while ((pdir = readdir(dir)))  {
	  TString foldName=pdir->d_name;//data16-new.root
	  //std::cout<<"foldName == "<<foldName<<"\n";
      if(foldName!=DSIDStr) continue; // Yue
	  std::cout <<"input files is "<< pdir->d_name << std::endl;
      DIR*     dir2;
      dirent*  pdir2;
      dir2 = opendir( (m_inputPath+"/"+foldName) );     // open current directory
      if (!foldName.Contains("root")) continue;//Yanhui
	  m_inputChain->Add( ( m_inputPath+"/"+foldName) );

	  /*
	  while ((pdir2 = readdir(dir2)))  {
	TString fName=pdir2->d_name;
	//cout << " fName: " << fName << endl;
	if (!fName.Contains("root")) continue;//Yanhui
	TString temp = m_inputPath+"/"+foldName+"/"+fName;
	m_inputChain->Add( ( m_inputPath+"/"+foldName+"/"+fName) );
	if(m_Debug) std::cout << fName << std::endl;
	TFile* loadFile = TFile::Open( m_inputPath+"/"+foldName+"/"+fName );
	h_SumW = (TH1F*) loadFile->Get(SumWName);
	h_XSec = (TH1F*) loadFile->Get(XSecName);
	m_SumWeights += h_SumW->GetBinContent(1);
	if( m_XSec == 0 )     m_XSec = h_XSec->GetBinContent(1) / h_XSec->GetEntries();  //Need to divide by entries due to grid merging
      }
	  */

    }
  } 
  //std::cout << "m_SumWeights " << m_SumWeights << std::endl;
  //std::cout << "m_XSec " << m_XSec << std::endl;
      
}


void generateHistogram::FillHistograms(TString DSID){
//Dummy function to be overwritten
}


float generateHistogram::getTagEff(TLorentzVector jet, int jetFlav){

  if( jetFlav == 5 ) {
    return hTruthMap_b->GetBinContent(hTruthMap_b->GetXaxis()->FindBin(fabs(jet.Eta())), hTruthMap_b->GetYaxis()->FindBin(jet.Pt()));
  }
  else if ( jetFlav == 4 ) {
    return hTruthMap_c->GetBinContent(hTruthMap_c->GetXaxis()->FindBin(fabs(jet.Eta())), hTruthMap_c->GetYaxis()->FindBin(jet.Pt()));
  }
  else if ( jetFlav == 15 ) {
    return hTruthMap_t->GetBinContent(hTruthMap_t->GetXaxis()->FindBin(fabs(jet.Eta())), hTruthMap_t->GetYaxis()->FindBin(jet.Pt()));
  }
  else if ( jetFlav == 0 ) {
    return hTruthMap_l->GetBinContent(hTruthMap_l->GetXaxis()->FindBin(fabs(jet.Eta())), hTruthMap_l->GetYaxis()->FindBin(jet.Pt()));
  }
  else if ( jetFlav == -999 ) return 0.0;
  else{
    std::cout << "Jet flavour not found - exiting" << std::endl;
    exit(0);
  }
}

int generateHistogram::roll_dice(const std::vector<float>&choices){
  static TRandom3 rand;
  double total=0;
  rand.SetSeed(123456);
//  rand.SetSeed(time(NULL));
  for(auto choice:choices)total+=choice;
  double dice=rand.Rndm(),choice=total*dice,tat=0;
//   cout<<"total: "<<total<<", choice: "<<choice<<", dice: "<<dice<<endl;
  for(unsigned int i=0;i<choices.size();i++){
    tat+=choices.at(i);
    if(choice<tat)return i;
  }
  return choices.size()-1;
}

