#include "generateHistogram_3lep.h"
#include "histSvc.h"
#include "ReadTruthTree.h"
#include "MVATree.h"

#include <TFile.h>
#include <TH1.h>
#include <TH2.h>
#include <TH3.h>
#include <TKey.h>
#include <TList.h>
#include <TClass.h>
#include <TRegexp.h>
#include "TChain.h"
#include "TLorentzVector.h"

#include <iostream>
#include <fstream>
#include <vector>

generateHistogram_3lep::generateHistogram_3lep() {

}

generateHistogram_3lep::~generateHistogram_3lep() {

}

void generateHistogram_3lep::FillHistograms(TString DSID){
  Selection3Lepton(DSID);  //Needed to steer the code to run the 2-lepton selection
}

void generateHistogram_3lep::Selection3Lepton(TString DSID){

  int nEvents = ( m_nEvents == -1 ) ? m_inputChain->GetEntries() : m_nEvents;  // Reading in the number of events, and if set to run on a subset
  float m_weight = 1;

  int tenPercent = nEvents / 5;
  
  //TFile*FF_El = new TFile("data/3DFF_El.root");
  //TH3D*h3d_El = (TH3D*)FF_El->Get("FF_El");
  //TFile*FF_El = new TFile("data/FF_El.root");
  //TH2D*h2d_El = (TH2D*)FF_El->Get("FF_El");
  //TFile*FF_Mu = new TFile("data/FF_Mu.root");
  //TH2D*h2d_Mu = (TH2D*)FF_Mu->Get("FF_Mu");

    for( int i=0; i<nEvents; ++i ){
    //gDebug=2;
	readNtuple->GetEntry(i);  // Load the event
	m_weight = readNtuple->weight;
    if(tenPercent>0){
    if( i%tenPercent == 0 ) std::cout << " Running on event " << i << " of " << nEvents << std::endl;
	}
	unsigned long long RunNumber = readNtuple->runNumber;
	unsigned long long EventNumber = readNtuple->eventNumber;
	bool passGHHSS2lSR = readNtuple->passGHHSS2lSR;
	bool passSS2lSRreg1 = readNtuple->passSS2lSRreg1;
	bool passSS2lSRreg2 = readNtuple->passSS2lSRreg2;
	float fatjet1Pt = readNtuple->fatjet1Pt;
	float fatjet1M = readNtuple->fatjet1M;
	float met = readNtuple->met;
	float Mll = readNtuple->Mll;
	float Lep1Pt = readNtuple->Lep1Pt;
	float Lep2Pt = readNtuple->Lep2Pt;
	float Lep3Pt = readNtuple->Lep3Pt;
        float Lep1Eta = readNtuple->Lep1Eta;
        float Lep2Eta = readNtuple->Lep2Eta;
        float Lep3Eta = readNtuple->Lep3Eta;
	float Jet1Pt = readNtuple->Jet1Pt;
	float Jet2Pt = readNtuple->Jet2Pt;
	float Jet1Eta = readNtuple->Jet1Eta;
	float Jet2Eta = readNtuple->Jet2Eta;
        float Mjj = readNtuple->Mjj;
	int Nbjets = readNtuple->Nbjets;
	//bool Lep1isTight = readNtuple->Lep1isTight;
	//bool Lep2isTight = readNtuple->Lep2isTight;

	bool Lep1isLoose = readNtuple->Lep1isLoose;
	bool Lep1isNoBL = readNtuple->Lep1isNoBL;
	bool Lep2isLoose = readNtuple->Lep2isLoose;
	bool Lep2isNoBL = readNtuple->Lep2isNoBL;
    int  Lep1Flav = readNtuple->Lep1Flav;
	int  Lep2Flav = readNtuple->Lep2Flav;
	float  mV3 = readNtuple->mV3;
    float dPhill = readNtuple->dPhill;
	bool passTrig = readNtuple->passTrig;
	bool passTrigMatch = readNtuple->passTrigMatch;
	int  Nleptons  = readNtuple->Nleptons;

    bool pass4Lepton = readNtuple->pass4Lepton;
	int  Lep3Flav = readNtuple->Lep3Flav;
	int  Nel = readNtuple->Nel;
	int  Nmu = readNtuple->Nmu;
	bool Lep3isLoose = readNtuple->Lep3isLoose;
	bool Lep3isNoBL = readNtuple->Lep3isNoBL;
	bool passLeptonPt = readNtuple->passLeptonPt;
	float Mlll = readNtuple->Mlll;
	int Lep1Q = readNtuple->Lep1Q;
	int Lep2Q = readNtuple->Lep2Q;
    int Njets = readNtuple->Njets;
	bool pass3LRegion = readNtuple->pass3LRegion; // Nleptons==3 and one SFOS leptons.
	float Reg3lMll = readNtuple->Reg3lMll; //Mll(Z).
    float Reg3lMhh = readNtuple->Reg3lMhh;
	float Reg3lPthh = readNtuple->Reg3lPthh;
	float Reg3lPtL3 = readNtuple->Reg3lPtL3;
	int   region3l  = readNtuple->region3l;
	float DRlllv = readNtuple->DRlllv;
	float DRlljj = readNtuple->DRlljj;
	float DRllJ = readNtuple->DRllJ;
	float DRlvjj = readNtuple->DRlvjj;
	float DRlvJ = readNtuple->DRlvJ;
	float mHHllJ = readNtuple->mHHllJ;
	float ptHHllJ = readNtuple->ptHHllJ;
	float mHHlvJ = readNtuple->mHHlvJ;
	float ptHHlvJ = readNtuple->ptHHlvJ;
	float mHHlljj = readNtuple->mHHlljj;
	float ptHHlljj = readNtuple->ptHHlljj;
	float mHHlvjj = readNtuple->mHHlvjj;
	float ptHHlvjj = readNtuple->ptHHlvjj;
	float mWlv = readNtuple->mWlv;
	float pTWlv = readNtuple->pTWlv;
	float mZll = readNtuple->mZll;
	float pTZll = readNtuple->pTZll;
    int Nfatjets = readNtuple->Nfatjets;
        float fatjet1Eta = readNtuple->fatjet1Eta;
	bool fj1_isWJet80 = readNtuple->fj1_isWJet80;
        bool pass0SFOS = readNtuple->pass0SFOS;
	float mHH;
	float ptHH;
	float mV;
	float ptV;

        TString regionName = "WZ"; // WZ, WWW
	//basic selections
	if(!(passTrig && passTrigMatch)) continue;
	if(!(Nleptons==3)) continue; // 3 good leptons isTight or isLoose or isNoBL for electreon.
	if(pass4Lepton) continue; // 4th lepton veto
        if(regionName=="WZ"){
	  if(!pass3LRegion) continue; //(muon_num + elec_num) == 3 && ((abs(elecCS) == 0 && abs(muonCS) == 1) || (abs(muonCS) == 0 && abs(elecCS) == 1));  so one OSSF paril of letpons
	}
        if(!(Nbjets==0)) continue;
	if(!(passLeptonPt)) continue;

     
        if(Lep1isLoose || Lep1isNoBL) continue;
	if(Lep2isLoose || Lep2isNoBL) continue;
        if(Lep3isLoose || Lep3isNoBL) continue;

        if(regionName=="WZ") {
          //if(mZll<80e3 || mZll>100e3) continue;
          if (Mlll <= 110.e3) continue;
        }
        else if(regionName=="WWW") {
          if(!pass0SFOS) continue;
        }

	if(regionName=="WZ"){
          if(met<=40.e3) continue;
        }

	TString description;
//	if (Nfatjets>=1 && DRllJ<DRlllv && DRllJ<DRlvJ){ // so H->ZZ->Jll
//	   description = "3l_SR1";
//	   mHH  = mHHllJ;
//	   ptHH = ptHHllJ;
//	   mV = mWlv;
//	   ptV = pTWlv;
//	}
//	else if (Njets>=2 && Jet1Pt>30e3 && Jet2Pt>20e3 && fabs(Jet1Eta)<2.5 && fabs(Jet2Eta)<2.5 && DRlljj<DRlllv && DRlljj < DRlvjj){ //so H->ZZ->jjll
//	  description = "3l_SR2";
//	  mHH = mHHlljj;
//	  ptHH = ptHHlljj;
//	  mV = mWlv;
//	  ptV = pTWlv;
//	}
//	else if (Nfatjets>=1 && DRlvJ<DRlllv && DRlvJ < DRllJ){ // so H->WW->Jlv
//	  description = "3l_SR3";
//	  mHH  = mHHlvJ;
//	  ptHH = ptHHlvJ;
//	  mV = mZll;
//	  ptV = pTZll;
//	}
//	else if (Njets>=2 && Jet1Pt>30e3 && Jet2Pt>20e3 && fabs(Jet1Eta)<2.5 && fabs(Jet2Eta)<2.5 && DRlvjj < DRlllv && DRlvjj < DRlljj){  //so H->WW->jjlv
//	  description = "3l_SR4";
//	  mHH  = mHHlvjj;
//	  ptHH = ptHHlvjj;
//	  mV = mZll;
//	  ptV = pTZll;
//	}
//	//if(!(ptHH>300e3 && ptV>300e3)) continue;
//	if(!(ptV>300e3)) continue;
	/*
	if(region3l==1) description = "3l_SR1"; //H->ZZ->Jll
	if(region3l==2) description = "3l_SR2";//H->ZZ->jjjj
	if(region3l==3) description = "3l_SR3";//H->WW->Jlv
	if(region3l==4) description = "3l_SR4";//h->WW->jjlv
	*/
	/*
	if(!(Lep3isLoose && Lep3isNoBL)) description = "3l_SR";
	if(!Lep3isLoose && Lep3isNoBL) description = "3l_CR";
    */

        bool pass3lCRBoosted = (Nfatjets>=1 && fatjet1Pt >200e3 && fatjet1M >50.e3 && fatjet1M<200.e3); 
        bool pass3lCRResolved = (!pass3lCRBoosted && Njets>=2 && Jet1Pt>20e3 && Jet2Pt>20e3 && fabs(Jet1Eta)<2.5 && fabs(Jet2Eta)<2.5); 
       if(regionName=="WZ") {
        if( !(pass3lCRBoosted || pass3lCRResolved) ) continue;
       }
       float Meff = -1.; float nbins = 5; float xMin = 400.; float xMax = 2000.;
       float norma = 1.0;
       if( DSID.Contains("GHH") ) m_weight *= 1.3;
       if(pass3lCRBoosted) {
          if(regionName=="WZ") description = "CR3lBoosted";
          else if(regionName=="WWW") description = "CRWWW3lBoosted";
          Meff = fatjet1Pt + met + Lep1Pt + Lep2Pt + Lep3Pt;
          nbins = 120; xMin = 200.; xMax = 3000.;
          } 
       else if (pass3lCRResolved) {
          if(regionName=="WZ") description = "CR3lResolved";	
          else if(regionName=="WWW") description = "CRWWW3lResolved";	
          Meff = Jet1Pt + Jet2Pt + met + Lep1Pt + Lep2Pt + Lep3Pt;
          nbins = 120; xMin = 200.; xMax = 3000.;
          }
   
       TString s_NP_lep="";
       if(m_MJ){
         if(Nel==3 && Nmu==0) {s_NP_lep="ElNom_";}
         else if(Nel==2 && Nmu==1) {s_NP_lep="MuNom_";}
         else if(Nel==1 && Nmu==2) {s_NP_lep="ElNom_";}
         else if(Nel==0 && Nmu==3) {s_NP_lep="MuNom_";}
      }


       m_histFill->SetDescription(s_NP_lep+description+"Inc");
   //Set the region description
    //m_histFill->SetNjets(nJets);
    //m_histFill->SetNFjets(nFJ);
    //m_histFill->SetpTV(pTV);  
	//m_histFill->SetNtags(nTags);
    bool isEl = false;
	//if (nElectrons==1) isEl = true;
	bool isCBA = false;
	//if(m_CBA) isCBA = true;
    //Fill the histograms
    
	if(m_Debug) std::cout << "Filling histograms" << std::endl;
        //m_histFill->BookFillHist("mV", isEl,isCBA,20,  0,200,  mV/1e3,    m_weight);//mll Z->ll
	//m_histFill->BookFillHist("ptV", isEl,isCBA,300,  0,3000,  ptV/1e3,    m_weight);//mll Z->ll
	//m_histFill->BookFillHist("mll", isEl,isCBA,300,  0,3000,  Mll/1e3,    m_weight);//Leading two leptons
	//m_histFill->BookFillHist("mllZ", isEl,isCBA,300,  0,3000,  Reg3lMll/1e3,    m_weight);//mll Z->ll
	//m_histFill->BookFillHist("mlll", isEl,isCBA,300,  0,3000,  Mlll/1e3,    m_weight);
	//m_histFill->BookFillHist("Mhh", isEl,isCBA,300,  0,3000,  Reg3lMhh/1e3,    m_weight);
        //m_histFill->BookFillHist("Pthh", isEl,isCBA,300,  0,3000,  Reg3lPthh/1e3,    m_weight);
        //m_histFill->BookFillHist("mHH", isEl,isCBA,300,  0,3000,  mHH/1e3,    m_weight);
	//m_histFill->BookFillHist("ptHH", isEl,isCBA,300,  0,3000,  ptHH/1e3,    m_weight);
	//m_histFill->BookFillHist("mV3", isEl,isCBA,60,   0,300,   mV3/1e3,    m_weight);
        //m_histFill->BookFillHist("Lep1Pt", isEl,isCBA,200,   0,2000,   Lep1Pt/1e3,    m_weight);
	//m_histFill->BookFillHist("Lep2Pt", isEl,isCBA,200,   0,2000,   Lep2Pt/1e3,    m_weight);
	//m_histFill->BookFillHist("Lep3Pt", isEl,isCBA,200,   0,2000,   Lep3Pt/1e3,    m_weight);
	m_histFill->BookFillHist("Norm", isEl,isCBA,1,   0.5,1.5,   norma,    m_weight);
        m_histFill->BookFillHist("Meff", isEl,isCBA,nbins,   xMin,xMax,   Meff/1e3,    m_weight);
        m_histFill->BookFillHist("Njets", isEl,isCBA,11,   0,11,   Njets,    m_weight);
        m_histFill->BookFillHist("Nbjets", isEl,isCBA,11,   0,11,   Nbjets,    m_weight);
        m_histFill->BookFillHist("mllZ" , isEl,isCBA,12,  60,120,  Reg3lMll/1e3,    m_weight);
        m_histFill->BookFillHist("mlll", isEl,isCBA,20,  0,800,  Mlll/1e3,    m_weight);
        m_histFill->BookFillHist("Lep1Pt", isEl,isCBA,20,   0,600,   Lep1Pt/1e3,    m_weight);
        m_histFill->BookFillHist("Lep2Pt", isEl,isCBA,20,   0,400,   Lep2Pt/1e3,    m_weight);
        m_histFill->BookFillHist("Lep3Pt", isEl,isCBA,20,   0,400,   Lep3Pt/1e3,    m_weight);
        m_histFill->BookFillHist("jet1Pt", isEl,isCBA,20,   0,800,   Jet1Pt/1e3,    m_weight);
        m_histFill->BookFillHist("jet2Pt", isEl,isCBA,20,   0,600,   Jet2Pt/1e3,    m_weight);
        m_histFill->BookFillHist("fatjet1Pt", isEl,isCBA,20,   0,1000,   fatjet1Pt/1e3,    m_weight);
        m_histFill->BookFillHist("fatjet1M", isEl,isCBA,20,   0,200,   fatjet1M/1e3,    m_weight);
	m_histFill->BookFillHist("MET", isEl,isCBA,15,  0,600,  met/1e3,    m_weight);
        m_histFill->BookFillHist("jet1Eta", isEl,isCBA,24,   -3,3,   Jet1Eta,    m_weight);
        m_histFill->BookFillHist("jet2Eta", isEl,isCBA,24,   -3,3,   Jet2Eta,    m_weight);
        m_histFill->BookFillHist("Lep1Eta", isEl,isCBA,24,   -3,3,   Lep1Eta,    m_weight);
        m_histFill->BookFillHist("Lep2Eta", isEl,isCBA,24,   -3,3,   Lep2Eta,    m_weight);
        m_histFill->BookFillHist("Lep3Eta", isEl,isCBA,24,   -3,3,   Lep3Eta,    m_weight);
        m_histFill->BookFillHist("fatjet1Eta", isEl,isCBA,20,   -2.5,2.5,   fatjet1Eta,    m_weight);
        m_histFill->BookFillHist("Mjj", isEl,isCBA,40,   0,1200,   Mjj/1e3,    m_weight);

        // flavor
        TString flav;
        if(Nel==3 && Nmu==0) flav = "eee";
        else if(Nel==2 && Nmu==1) flav = "eemu";
        else if(Nel==1 && Nmu==2) flav = "emumu";
        else if(Nel==0 && Nmu==3) flav = "mumumu";
        m_histFill->SetDescription(s_NP_lep+description+flav); 
        m_histFill->BookFillHist("Meff", isEl,isCBA,nbins,   xMin,xMax,   Meff/1e3,    m_weight);
        m_histFill->BookFillHist("Njets", isEl,isCBA,11,   0,11,   Njets,    m_weight);
        m_histFill->BookFillHist("Nbjets", isEl,isCBA,11,   0,11,   Nbjets,    m_weight);
        m_histFill->BookFillHist("mllZ" , isEl,isCBA,12,  60,120,  Reg3lMll/1e3,    m_weight);
        m_histFill->BookFillHist("mlll", isEl,isCBA,20,  0,800,  Mlll/1e3,    m_weight);
        m_histFill->BookFillHist("Lep1Pt", isEl,isCBA,20,   0,600,   Lep1Pt/1e3,    m_weight);
        m_histFill->BookFillHist("Lep2Pt", isEl,isCBA,20,   0,400,   Lep2Pt/1e3,    m_weight);
        m_histFill->BookFillHist("Lep3Pt", isEl,isCBA,20,   0,400,   Lep3Pt/1e3,    m_weight);
        m_histFill->BookFillHist("jet1Pt", isEl,isCBA,20,   0,800,   Jet1Pt/1e3,    m_weight);
        m_histFill->BookFillHist("jet2Pt", isEl,isCBA,20,   0,600,   Jet2Pt/1e3,    m_weight);
        m_histFill->BookFillHist("fatjet1Pt", isEl,isCBA,20,   0,1000,   fatjet1Pt/1e3,    m_weight);
        m_histFill->BookFillHist("fatjet1M", isEl,isCBA,20,   0,200,   fatjet1M/1e3,    m_weight);
	m_histFill->BookFillHist("MET", isEl,isCBA,15,  0,600,  met/1e3,    m_weight);
        m_histFill->BookFillHist("jet1Eta", isEl,isCBA,24,   -3,3,   Jet1Eta,    m_weight);
        m_histFill->BookFillHist("jet2Eta", isEl,isCBA,24,   -3,3,   Jet2Eta,    m_weight);
        m_histFill->BookFillHist("Lep1Eta", isEl,isCBA,24,   -3,3,   Lep1Eta,    m_weight);
        m_histFill->BookFillHist("Lep2Eta", isEl,isCBA,24,   -3,3,   Lep2Eta,    m_weight);
        m_histFill->BookFillHist("Lep3Eta", isEl,isCBA,24,   -3,3,   Lep3Eta,    m_weight);
        m_histFill->BookFillHist("fatjet1Eta", isEl,isCBA,20,   -2.5,2.5,   fatjet1Eta,    m_weight);
        m_histFill->BookFillHist("Mjj", isEl,isCBA,40,   0,1200,   Mjj/1e3,    m_weight);
        m_histFill->BookFillHist("Norm", isEl,isCBA,1,   0.5,1.5,   norma,    m_weight);

        // can do better but for now I just call the BookFillHist twice to get the inclusive and separated histograms
	//if( (pass3lCRBoosted || pass3lCRResolved) ) description = "CR3lBoostedCR3lResolved";
	if( regionName=="WZ") {
         //if((pass3lCRBoosted || pass3lCRResolved)) description = "CR3l";
         if((pass3lCRBoosted || pass3lCRResolved)) description = "CR3lBoostedIncCR3lResolved";
         }
        else if(regionName=="WWW") description = "CRWWW3l";
	nbins = 120; xMin = 200.; xMax = 3000.;
        m_histFill->SetDescription(s_NP_lep+description+"Inc");
        m_histFill->BookFillHist("Norm", isEl,isCBA,1,   0.5,1.5,   norma,    m_weight);
        m_histFill->BookFillHist("Meff", isEl,isCBA,nbins,   xMin,xMax,   Meff/1e3,    m_weight);
        m_histFill->BookFillHist("Njets", isEl,isCBA,11,   0,11,   Njets,    m_weight);
        m_histFill->BookFillHist("Nbjets", isEl,isCBA,11,   0,11,   Nbjets,    m_weight);
        m_histFill->BookFillHist("mllZ" , isEl,isCBA,12,  60,120,  Reg3lMll/1e3,    m_weight);
        m_histFill->BookFillHist("mlll", isEl,isCBA,20,  0,800,  Mlll/1e3,    m_weight);
        m_histFill->BookFillHist("Lep1Pt", isEl,isCBA,20,   0,600,   Lep1Pt/1e3,    m_weight);
        m_histFill->BookFillHist("Lep2Pt", isEl,isCBA,20,   0,400,   Lep2Pt/1e3,    m_weight);
        m_histFill->BookFillHist("Lep3Pt", isEl,isCBA,20,   0,400,   Lep3Pt/1e3,    m_weight);
        m_histFill->BookFillHist("jet1Pt", isEl,isCBA,20,   0,800,   Jet1Pt/1e3,    m_weight);
        m_histFill->BookFillHist("jet2Pt", isEl,isCBA,20,   0,600,   Jet2Pt/1e3,    m_weight);
        m_histFill->BookFillHist("fatjet1Pt", isEl,isCBA,20,   0,1000,   fatjet1Pt/1e3,    m_weight);
        m_histFill->BookFillHist("fatjet1M", isEl,isCBA,20,   0,200,   fatjet1M/1e3,    m_weight);
        m_histFill->BookFillHist("MET", isEl,isCBA,15,  0,600,  met/1e3,    m_weight);
        m_histFill->BookFillHist("jet2Eta", isEl,isCBA,24,   -3,3,   Jet2Eta,    m_weight);
        m_histFill->BookFillHist("Lep1Eta", isEl,isCBA,24,   -3,3,   Lep1Eta,    m_weight);
        m_histFill->BookFillHist("Lep2Eta", isEl,isCBA,24,   -3,3,   Lep2Eta,    m_weight);
        m_histFill->BookFillHist("Lep3Eta", isEl,isCBA,24,   -3,3,   Lep3Eta,    m_weight);
        m_histFill->BookFillHist("fatjet1Eta", isEl,isCBA,20,   -2.5,2.5,   fatjet1Eta,    m_weight);
        m_histFill->BookFillHist("Mjj", isEl,isCBA,40,   0,1200,   Mjj/1e3,    m_weight);
       
  }
}


