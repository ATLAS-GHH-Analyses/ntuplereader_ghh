#include "generateHistogram_2lep.h"
#include "histSvc.h"
#include "ReadTruthTree.h"
#include "MVATree.h"

#include <TFile.h>
#include <TH1.h>
#include <TH2.h>
#include <TH3.h>
#include <TKey.h>
#include <TList.h>
#include <TClass.h>
#include <TRegexp.h>
#include "TChain.h"
#include "TLorentzVector.h"

#include <iostream>
#include <fstream>
#include <vector>

generateHistogram_2lep::generateHistogram_2lep() {

}

generateHistogram_2lep::~generateHistogram_2lep() {

}

void generateHistogram_2lep::FillHistograms(TString DSID){
  Selection2Lepton(DSID);  //Needed to steer the code to run the 2-lepton selection
}

void generateHistogram_2lep::Selection2Lepton(TString DSID){

  int nEvents = ( m_nEvents == -1 ) ? m_inputChain->GetEntries() : m_nEvents;  // Reading in the number of events, and if set to run on a subset
  float m_weight = 1;

  int tenPercent = nEvents / 5;

  TFile*CF = new TFile("data/CFRates_data_nominal.root");//nominal
  //TFile*CF = new TFile("data/CFRates_Zjets.root");//sys
  //TFile*CF = new TFile("data/CFRates_ZmassWindownPlus2.root");//sys
  //TFile*CF = new TFile("data/CFRates_ZmassWindownMinus2.root");//sys
  //TFile*CF = new TFile("data/CFRates_MCSubtraction.root");//sys
    for( int i=0; i<nEvents; ++i ){
    //gDebug=2;
	readNtuple->GetEntry(i);  // Load the event
	m_weight = readNtuple->weight;
    if(tenPercent>0){
    if( i%tenPercent == 0 ) std::cout << " Running on event " << i << " of " << nEvents << std::endl;
	}
	unsigned long long RunNumber = readNtuple->runNumber;
	unsigned long long EventNumber = readNtuple->eventNumber;
	if (DSID == "ssWW") {
	  bool isWW = readNtuple->isWW;
	  if (!isWW) continue; //remove triboson
	}
	bool passGHHSS2lSR = readNtuple->passGHHSS2lSR;
	bool passSS2lSRreg1 = readNtuple->passSS2lSRreg1;
	bool passSS2lSRreg2 = readNtuple->passSS2lSRreg2;
        float fatjet1Pt = readNtuple->fatjet1Pt;
        float fatjet1Eta = readNtuple->fatjet1Eta;
        float fatjet1Phi = readNtuple->fatjet1Phi;
        float fatjet2M = readNtuple->fatjet2M;
        float fatjet2Pt = readNtuple->fatjet2Pt;
        float fatjet2Eta = readNtuple->fatjet2Eta;
        float fatjet2Phi = readNtuple->fatjet2Phi;
	float met = readNtuple->met;
	float metSig = readNtuple->metSig;
	float metSigRatio = readNtuple->metSigRatio;
	float Mll = readNtuple->Mll;
	float Lep1Pt = readNtuple->Lep1Pt;
	float Lep2Pt = readNtuple->Lep2Pt;
	float Lep1Eta = readNtuple->Lep1Eta;
	float Lep2Eta = readNtuple->Lep2Eta;
        float Jet1Pt = readNtuple->Jet1Pt;
        float Jet1Eta = readNtuple->Jet1Eta;
        float Jet1Phi = readNtuple->Jet1Phi;
        float Jet1E = readNtuple->Jet1E;
        float Jet2Pt = readNtuple->Jet2Pt;
        float Jet2Eta = readNtuple->Jet2Eta;
        float Jet2Phi = readNtuple->Jet2Phi;
        float Jet2E = readNtuple->Jet2E;
	float Nbjets = readNtuple->Nbjets;
	float Nbjets77 = readNtuple->Nbjets77;
	bool Lep1isTight = readNtuple->Lep1isTight;
	bool Lep2isTight = readNtuple->Lep2isTight;

	bool Lep1isLoose = readNtuple->Lep1isLoose;
	bool Lep1isNoBL = readNtuple->Lep1isNoBL;
	bool Lep2isLoose = readNtuple->Lep2isLoose;
	bool Lep2isNoBL = readNtuple->Lep2isNoBL;
        int  Lep1Flav = readNtuple->Lep1Flav;
	int  Lep2Flav = readNtuple->Lep2Flav;
        int  Lep1Q = readNtuple->Lep1Q;
        int  Lep2Q = readNtuple->Lep2Q;
	float  mV3 = readNtuple->mV3;
	float  PtV3 = readNtuple->PtV3;
    float dPhill = readNtuple->dPhill;
        float dEtall = readNtuple->dEtall;
       float dRll = sqrt(dPhill*dPhill+dEtall*dEtall);
    bool passTrig = readNtuple->passTrig;
    bool passTrigMatch = readNtuple->passTrigMatch;
	int  Nleptons  = readNtuple->Nleptons;
	bool passSameSign = readNtuple->passSameSign;
	bool pass3Lepton = readNtuple->pass3Lepton;
	int Nfatjets = readNtuple->Nfatjets;
    float fatjet1M = readNtuple->fatjet1M;
	float Mjj = readNtuple->Mjj;
	float Ptjj = readNtuple->Ptjj;
	int Njets = readNtuple->Njets;
	int regionSS2l = readNtuple->regionSS2l;
	float fj1_SmoothWContained80_SF = readNtuple->fj1_SmoothWContained80_SF;
	bool fj1_isWJet80 = readNtuple->fj1_isWJet80;
	bool passLeptonPt = readNtuple->passLeptonPt;
	bool Lep1isSignalL = readNtuple->Lep1isSignalL;
	bool Lep1isECIDSL = readNtuple->Lep1isECIDSL;
	bool Lep1isPLVTightL = readNtuple->Lep1isPLVTightL;
	bool Lep2isSignalL = readNtuple->Lep2isSignalL;
	bool Lep2isECIDSL = readNtuple->Lep2isECIDSL;
	bool Lep2isPLVTightL = readNtuple->Lep2isPLVTightL;
	float Lep1d0 = readNtuple->Lep1d0; // well, not saved in the ntuples..
	float Lep2d0 = readNtuple->Lep2d0; // well, not saved in the ntuples..
	int Lep1truthStatus = readNtuple->Lep1truthStatus;
	int Lep2truthStatus = readNtuple->Lep2truthStatus;
	bool pass4Lepton = readNtuple->pass4Lepton;
	bool pass3LRegion = readNtuple->pass3LRegion;

	float mTWL1 = readNtuple->mTWL1;
	float mTWL2 = readNtuple->mTWL2;

	float mTWMax, mTWMin;
	if(mTWL1>mTWL2) { mTWMax = mTWL1; mTWMin = mTWL2;}
	else if (mTWL1<mTWL2) { mTWMax = mTWL2; mTWMin = mTWL1;}

	float dPhiL1FJ1 = readNtuple->dPhiL1FJ1;
	float dPhiL2FJ1 = readNtuple->dPhiL2FJ1;
	float dEtaL1FJ1 = readNtuple->dEtaL1FJ1;
	float dEtaL2FJ1 = readNtuple->dEtaL2FJ1;
	float dRL1FJ1 = readNtuple->dRL1FJ1;
	float dRL2FJ1 = readNtuple->dRL2FJ1;

    float dPhiL1J1 = readNtuple->dPhiL1J1;
    float dPhiL2J1 = readNtuple->dPhiL2J1;
    float dEtaL1J1 = readNtuple->dEtaL1J1;
    float dEtaL2J1 = readNtuple->dEtaL2J1;
    float dRL1J1 = readNtuple->dRL1J1;
    float dRL2J1 = readNtuple->dRL2J1;

    float dPhiL1J2 = readNtuple->dPhiL1J2;
    float dPhiL2J2 = readNtuple->dPhiL2J2;
    float dEtaL1J2 = readNtuple->dEtaL1J2;
    float dEtaL2J2 = readNtuple->dEtaL2J2;
    float dRL1J2 = readNtuple->dRL1J2;
    float dRL2J2 = readNtuple->dRL2J2;

    float dPhiL1W = readNtuple->dPhiL1W;
    float dPhiL2W = readNtuple->dPhiL2W;
    float dEtaL1W = readNtuple->dEtaL1W;
    float dEtaL2W = readNtuple->dEtaL2W;
    float dRL1W = readNtuple->dRL1W;
    float dRL2W = readNtuple->dRL2W;

	float dPhiJ1J2 = readNtuple->dPhiJ1J2;
	float dEtaJ1J2 = readNtuple->dEtaJ1J2;
	float dRJ1J2 = readNtuple->dRJ1J2;

    int Lep1Author = readNtuple->Lep1Author;
	int Lep1AddAmbiguity = readNtuple->Lep1AddAmbiguity;
	int Lep2Author = readNtuple->Lep2Author;
	int Lep2AddAmbiguity = readNtuple->Lep2AddAmbiguity;
        int Lep3Author = readNtuple->Lep3Author;
     	int Lep3AddAmbiguity = readNtuple->Lep3AddAmbiguity;

	float MinidPhiLJ=5.0;
	if(fabs(dPhiL1J1)<fabs(dPhiL1J2))   MinidPhiLJ = fabs(dPhiL1J1);
	else MinidPhiLJ = fabs(dPhiL1J2);
	if(fabs(dPhiL2J1)<MinidPhiLJ) MinidPhiLJ = fabs(dPhiL2J1);
	if(fabs(dPhiL2J2)<MinidPhiLJ) MinidPhiLJ = fabs(dPhiL2J2);
	float MinidPhiLFJ =5.0;
	if(fabs(dPhiL1FJ1)<fabs(dPhiL2FJ1)) MinidPhiLFJ = fabs(dPhiL1FJ1);
	else MinidPhiLFJ = fabs(dPhiL2FJ1);

    float MinidEtaLJ=5.0;
    if(dEtaL1J1<dEtaL1J2)   MinidEtaLJ = dEtaL1J1;
    else MinidEtaLJ = dEtaL1J2;
    if(dEtaL2J1<MinidEtaLJ) MinidEtaLJ = dEtaL2J1;
    if(dEtaL2J2<MinidEtaLJ) MinidEtaLJ = dEtaL2J2;
    float MinidEtaLFJ =5.0;
    if(dEtaL1FJ1<dEtaL2FJ1) MinidEtaLFJ = dEtaL1FJ1;
    else MinidEtaLFJ = dEtaL2FJ1;

    float MinidRLJ=5.0;
    if(dRL1J1<dRL1J2)   MinidRLJ = dRL1J1;
    else MinidRLJ = dRL1J2;
    if(dRL2J1<MinidRLJ) MinidRLJ = dRL2J1;
    if(dRL2J2<MinidRLJ) MinidRLJ = dRL2J2;
    float MinidRLFJ =5.0;
    if(dRL1FJ1<dRL2FJ1) MinidRLFJ = dRL1FJ1;
    else MinidRLFJ = dRL2FJ1; 
	//Which one we would like to run?
	//if(Lep1Flav==2 && !Lep1isLoose){
	//std::cout<<"Lep1isPLVTightL "<<Lep1isPLVTightL<<"\n";
	//}
	//bool RunSubtraction = true;
	//Nominal, CR1/2/3 in term of the lepton selection
    bool RunNominal = true; //SR lepton selection
    bool RunSideBand = m_runSideBand; //VR // false it for CR and SR
    bool RunssWW = m_runssWW; //ssWW  CR
    bool RunssWW500 = false; //ssWW Mjj>500GeV CR
    bool RunOS = false; //CF
    bool RunCR1 = false; // nonprompt, RunNominal = false
    bool RunCR2 = false;
    bool RunCR3 = false;
    bool RunVgammaCR1 = false; // photon conversion, RunNominal = false    
    bool Lep1isAntiID = false;
    bool Lep2isAntiID = false;
    bool Lep1isBLID = false;
    bool Lep2isBLID = false;
	//please set m_MJ=true only when run CR1, CR2 and CR3, and do the Subtraction.
	if(m_MJ && !RunCR2 &&!RunCR3 && !RunCR1) {
	 std::cout<<"Please set m_MJ=true only when run CR1 or CR2 or CR3"<<"\n";
	 continue;
	}
	if( (m_MJ||m_Vgamma) && !m_DSID.Contains("data")) m_weight *= -1; //non-prompt or Vgamma
	if(Lep1Flav==1){ //L1 electron
	 Lep1isAntiID = ( Lep1isLoose /*&& Lep1isSignalL*/ && Lep1isECIDSL );
	 Lep1isBLID = (Lep1isNoBL /*&& Lep1Author!=1 && Lep1AddAmbiguity>0*/); //for only electron, muon always false.
	}
	else if (Lep1Flav==2){ //L1 muon
	 Lep1isAntiID = ( Lep1isLoose /*&& Lep1isSignalL*/);
	}

    if(Lep2Flav==1){ //L2 electron
     Lep2isAntiID = ( Lep2isLoose /*&& Lep2isSignalL*/ && Lep2isECIDSL );
	 Lep2isBLID = (Lep2isNoBL /*&& Lep2Author!=1 && Lep2AddAmbiguity>0*/);
    }
    else if (Lep2Flav==2){ //L2 muon
     Lep2isAntiID = ( Lep2isLoose /*&& Lep2isSignalL*/);
    }
    float AntiIDLepPt =-99; 
    float BLIDLepPt =-99;
    int AntiIDLepFlav=-99;
	float AntiIDLepEta=-99;
    if(Lep1isAntiID) {
      AntiIDLepPt = Lep1Pt;
      AntiIDLepFlav = Lep1Flav;
      AntiIDLepEta = fabs(Lep1Eta);
    }
    else if(Lep2isAntiID){
      AntiIDLepPt = Lep2Pt;
      AntiIDLepFlav = Lep2Flav;
      AntiIDLepEta = fabs(Lep2Eta);
    }
    if(Lep1isBLID) BLIDLepPt = Lep1Pt;
    else if (Lep2isBLID) BLIDLepPt = Lep2Pt;

	if(!(passLeptonPt && passTrig && passTrigMatch && Nleptons==2 && !pass3Lepton)) continue;
	if(RunOS){
	 if(passSameSign) continue;
	}
	else {
	 if(!passSameSign) continue;
	}

	//SR selections
	if(RunNominal){
	 if(Lep1isLoose || Lep1isNoBL) continue;
	 if(Lep2isLoose || Lep2isNoBL) continue;
     //no Lep1isTight variables available in the trees!...
	 if(!(Nbjets==0)) continue; //Yanhui remember to change
	}
    //CR1 selections same as SR, but ask one ID and one AntiID leptons.
    //else in the Reader if not isTigt then judge if isLoose
    //isNoBL then !isTight and !isLoose
    else if(RunCR1){
     if (Lep1isNoBL || Lep2isNoBL) continue;//tight or loose
     if( ! ((Lep1isAntiID && Lep2isTight) || (Lep2isAntiID && Lep1isTight)) ) continue;
     if(!(Nbjets==0)) continue;
    }
    //CR2 selections same as SR, ask (two ID ) leptons, and Nbjets==1
    else if(RunCR2){
      if(!(Nbjets==1)) continue;  //Yanhui remember to change.
      if(Lep1isLoose || Lep1isNoBL) continue;
      if(Lep2isLoose || Lep2isNoBL) continue;
    }
    
    //CR3 selections same as SR, but ask one ID and one AntiID leptonsm and Bjets==1

    else if(RunCR3){
     if(!(Nbjets==1)) continue; //Yanhui remember
     if (Lep1isNoBL || Lep2isNoBL) continue; // tight or loose
     if( ! ((Lep1isAntiID && Lep2isTight) || (Lep2isAntiID && Lep1isTight)) ) continue;
    } 
   else if (RunVgammaCR1){
      if(!(Nbjets==0)) continue;
      if( ! ((Lep1isBLID && Lep2isTight) || (Lep2isBLID && Lep1isTight)) ) continue; // will remove the mumu events automatically.
   }
	
   TString s_NP_lep="";
   if(m_MJ && RunCR1){
     //use binned fake factor as nominal, electron pT binned; muon |eta| binned.
     if(AntiIDLepFlav==1){
       s_NP_lep="ElNom_";
       if(AntiIDLepPt<30e3) m_weight *= 0.027;
       else if(AntiIDLepPt<40e3) m_weight *= 0.051;
       else m_weight *= 0.139;
     }
     else if (AntiIDLepFlav==2){
       s_NP_lep="MuNom_";
       if(AntiIDLepEta<0.5) m_weight *= 0.024;
       else if(AntiIDLepEta<1.5) m_weight *= 0.016;
       else m_weight *= 0.011;
     }
     //use inclusive fake factor as systematics
	 /*
	 if(AntiIDLepFlav==1){
	  s_NP_lep="ElNom_";
	  m_weight *= 0.048;
	 }
	 else if (AntiIDLepFlav==2){
	  s_NP_lep="MuNom_";
	  m_weight *= 0.018;
	 }
	 */
   }
   if(m_Vgamma && RunVgammaCR1){
     //use inclusive fake rate as nominal
	 m_weight *= 0.43;
	 //use pT binned fake rates as systmatics, effec both shape and normalization.
	 //if(BLIDLepPt<25e3) m_weight *= 0.44;
	 //else m_weight *= 0.41;
   }


	//Let get the charge flip background in SS by: OS times CFRates.  
      float eff1=0.0; float eff2=0.0;
    if(RunOS){	
	TH2F*CF_hist = (TH2F*)CF->Get("CFRates");
	int maxBin = CF_hist->GetMaximumBin();  // get linearized bin containing maximum
    int etaBinMax, ptBinMax, z;
    CF_hist->GetBinXYZ(maxBin, etaBinMax, ptBinMax, z);           // get x and y coordinates for max bin
	float etaMax = CF_hist->GetXaxis()->GetBinCenter(etaBinMax);  // max eta value
    float ptMax = CF_hist->GetYaxis()->GetBinCenter(ptBinMax);    // max pt value
	float weight_CF=0.0;
	if(Lep1Flav==2 && Lep2Flav==2) continue; //mumu no charge flip
	else{
	 if(Lep1Flav==1){
	  int etaBin_1 = CF_hist->GetXaxis()->FindBin(min(fabs(Lep1Eta), etaMax));
	  int ptBin_1 = CF_hist->GetYaxis()->FindBin(min(Lep1Pt, ptMax));
	  eff1 = CF_hist->GetBinContent(etaBin_1, ptBin_1);
	 }
	 else eff1 =0;
	 if(Lep2Flav==1){
	  int etaBin_2 = CF_hist->GetXaxis()->FindBin(min(fabs(Lep2Eta), etaMax));
	  int ptBin_2 = CF_hist->GetYaxis()->FindBin(min(Lep2Pt, ptMax));
	  eff2 = CF_hist->GetBinContent(etaBin_2, ptBin_2);
	 }
	 else eff2 =0;
	}

	 weight_CF = 1 / (1 - eff1 - eff2 + (2 * eff1 * eff2)) - 1;  // scale factor for event
     m_weight *= weight_CF;
	 }
     
        float MJj1; float MJj2; float DYJj1; float DYJj2;
        float MJJ; float DYJJ; float DYjj; float DRJj1; float DRJj2;
        TLorentzVector v_J1; TLorentzVector v_J2;
        TLorentzVector v_j1; TLorentzVector v_j2;
        v_J1.SetPtEtaPhiM(fatjet1Pt,fatjet1Eta,fatjet1Phi,fatjet1M); v_J2.SetPtEtaPhiM(fatjet2Pt,fatjet2Eta,fatjet2Phi,fatjet2M);
        v_j1.SetPtEtaPhiE(Jet1Pt,Jet1Eta,Jet1Phi,Jet1E); v_j2.SetPtEtaPhiE(Jet2Pt,Jet2Eta,Jet2Phi,Jet2E);

        MJJ = Nfatjets>=2 ? (v_J1+v_J2).M() : -1.;
        DYJJ = Nfatjets>=2 ? fabs( v_J1.Rapidity() - v_J2.Rapidity()) : -1;
        DYjj = Njets>=2 ? fabs( v_j1.Rapidity() - v_j2.Rapidity()) : -1;
        MJj1 = Nfatjets>=1 && Njets>=1 ? (v_J1+v_j1).M() : -1.;
        MJj2 = Nfatjets>=1 && Njets>=2 ? (v_J1+v_j2).M() : -1.;
        DYJj1 = Nfatjets>=1 && Njets>=1 ? fabs( v_J1.Rapidity() - v_j1.Rapidity()) : -1;
        DYJj2 = Nfatjets>=1 && Njets>=2 ? fabs( v_J1.Rapidity() - v_j2.Rapidity()) : -1;
        DRJj1 = Nfatjets>=1 && Njets>=1 ? v_J1.DeltaR(v_j1) : -1;
        DRJj2 = Nfatjets>=1 && Njets>=2 ? v_J1.DeltaR(v_j2) : -1;
        dEtaJ1J2 = fabs(v_j1.Eta()-v_j2.Eta());
        float MJj_maxDY;
        if(Nfatjets>=1 && Njets>=1 && v_J1.DeltaR(v_j1) > 1.4) MJj_maxDY=(v_J1+v_j1).M();
        else if(Nfatjets>=1 && Njets>=2 && v_J1.DeltaR(v_j2) >1.4) MJj_maxDY=(v_J1+v_j2).M();
        else MJj_maxDY = -1.;

	//We use a loose cuts for CR2 and CR3, no WTagger/Wmass, no mll
	bool passSS2lBoosted = (Nfatjets>=1 && fatjet1Pt >200e3 && fatjet1M >50.e3 && fatjet1M<200.e3);
	bool passSS2lResolved = (!passSS2lBoosted && Njets>=2 && Jet1Pt>20e3 && Jet2Pt>20e3 && fabs(Jet1Eta)<2.5 && fabs(Jet2Eta)<2.5);
	bool passLep1Pt = Lep1Pt>27e3; //27 100
	//bool passMll = true;
	//if(Lep1Flav==1 && Lep2Flav==1 && Mll>80e3 && Mll<100e3) passMll = false;
	bool passMll = Mll>100e3;
	//bool passMET_M = ( (met>80e3) || (Lep1Flav==2 && Lep2Flav==2) ); // 80
	//bool passMET_R = ( (met>60e3) || (Lep1Flav==2 && Lep2Flav==2) );
	bool passMET_M = met>80e3;
	bool passMET_R = met>60e3;
	bool passWmass = Mjj>50e3 && Mjj<110e3; //50-110; 50-120;
	bool passmTW = mTWMax>150e3;
	bool passdEtaJ1J2 = dEtaJ1J2<1.5;
	bool pass2LMerged = false;
	bool pass2LResolved = false;
	//if(Njets<3) continue; //Yanhui remember
	if (RunSideBand){
	 pass2LMerged = (passSS2lBoosted && !fj1_isWJet80 && passLep1Pt && passMll && passMET_M /*&& passmTW*/);
	 pass2LResolved = (passSS2lResolved && passLep1Pt && passMll && passMET_R && !passWmass && Mjj<200.e3 /*&& passmTW && passdEtaJ1J2*/);
	}
	else if (RunssWW){
	 pass2LMerged = passSS2lBoosted && passLep1Pt && passMll && met>40e3;
	 pass2LResolved = passSS2lResolved && passLep1Pt && passMll && met>40e3 && Mjj >= 200.e3;
	}
	else if (RunssWW500){
	 pass2LMerged = passSS2lBoosted && passLep1Pt && passMll && met>40e3;
	 pass2LResolved = passSS2lResolved && passLep1Pt && passMll && met>40e3 && Mjj >= 500.e3;
	}
	else{
	 pass2LMerged = (passSS2lBoosted && fj1_isWJet80 && passLep1Pt && passMll && passMET_M /*&& passmTW*/);
	 pass2LResolved = (passSS2lResolved && passLep1Pt && passMll && passMET_R && passWmass /*&& passmTW && passdEtaJ1J2*/);
	}


    if(!(pass2LMerged || pass2LResolved)) continue;
    TString description; float nbins = 5; float xMin = 400.; float xMax = 2000.;
    float Meff; 
    if( DSID.Contains("GHH") ) m_weight *= 1.3;

    float SF_Wtagger = 1.;

    if( RunSideBand && pass2LMerged ) {
        SF_Wtagger = fj1_SmoothWContained80_SF;
       }; //VR
    if ( (RunNominal || RunCR1 || (m_Vgamma && RunVgammaCR1) ) && pass2LMerged && !RunSideBand && !RunssWW && !RunssWW500) {
        // std::cout << fj1_SmoothWContained80_SF << std::endl; 
         SF_Wtagger = fj1_SmoothWContained80_SF;
        };//SR

    float norma = 1.0;

        m_weight *= SF_Wtagger;

	if(pass2LMerged){
	 if( (RunNominal || RunCR1 || RunVgammaCR1) && !RunSideBand && !RunssWW && !RunssWW500) description = "SRSS2lBoosted";
	 else if(RunSideBand) description = "VRSS2lBoosted";
         else if(RunssWW) description = "ssWWBoosted";
         else if(RunssWW500) description = "ssWW500Boosted";
	 else description = "ss2l_SR";
	 mV3 = fatjet1M;
         PtV3 = fatjet1Pt;
	 Meff = PtV3 + met + Lep1Pt + Lep2Pt;
         nbins = 64; xMin = 400.; xMax = 2000.;
	}
	if(pass2LResolved){ 
	 if((RunNominal || RunCR1 || RunVgammaCR1) && !RunSideBand && !RunssWW && !RunssWW500) description = "SRSS2lResolved";
	 else if(RunSideBand) description = "VRSS2lResolved";
         else if(RunssWW) description = "ssWWResolved";
         else if(RunssWW500) description = "ssWW500Resolved";
	 else description = "ss2l_SR";
	 mV3  = Mjj;
	 PtV3 = Ptjj;
	 Meff = Jet1Pt + Jet2Pt + met + Lep1Pt + Lep2Pt;
         nbins = 32; xMin = 200.; xMax = 1000.;
	}
        m_histFill->SetDescription(s_NP_lep+description+"Inc");
   //Set the region description
    //m_histFill->SetNjets(nJets);
    //m_histFill->SetNFjets(nFJ);
    //m_histFill->SetpTV(pTV);  
	//m_histFill->SetNtags(nTags);
    bool isEl = false;
	//if (nElectrons==1) isEl = true;
	bool isCBA = false;
	//if(m_CBA) isCBA = true;
    //Fill the histograms
	//
    if(m_Debug) std::cout << "Filling histograms" << std::endl;
        m_histFill->BookFillHist("Norm", isEl,isCBA,1,   0.5,1.5,   norma,    m_weight);
        m_histFill->BookFillHist("Njets", isEl,isCBA,11,   0,11,   Njets,    m_weight);
	m_histFill->BookFillHist("Nbjets", isEl,isCBA,11,   0,11,   Nbjets,    m_weight);
	m_histFill->BookFillHist("mll", isEl,isCBA,40,  0,2000,  Mll/1e3,    m_weight); //5GeV
        m_histFill->BookFillHist("Lep1Pt", isEl,isCBA,20,   0,600,   Lep1Pt/1e3,    m_weight);
	m_histFill->BookFillHist("Lep2Pt", isEl,isCBA,20,   0,400,   Lep2Pt/1e3,    m_weight);
        if(RunssWW || RunssWW500) m_histFill->BookFillHist("Mjj", isEl,isCBA,40,   0,1600,   Mjj/1e3,    m_weight);
	else m_histFill->BookFillHist("Mjj", isEl,isCBA,20,   0,200,   Mjj/1e3,    m_weight);
        m_histFill->BookFillHist("DYjj", isEl,isCBA,40,   0,8,   DYjj,    m_weight);
	m_histFill->BookFillHist("Meff", isEl,isCBA,nbins,   xMin,xMax,   Meff/1e3,    m_weight);
	m_histFill->BookFillHist("jet1Pt", isEl,isCBA,20,   0,800,   Jet1Pt/1e3,    m_weight);
	m_histFill->BookFillHist("jet2Pt", isEl,isCBA,20,   0,600,   Jet2Pt/1e3,    m_weight);
	m_histFill->BookFillHist("fatjet1Pt", isEl,isCBA,20,   0,1000,   fatjet1Pt/1e3,    m_weight);
	m_histFill->BookFillHist("fatjet1M", isEl,isCBA,20,   0,200,   fatjet1M/1e3,    m_weight);
        m_histFill->BookFillHist("MET", isEl,isCBA,20,  0,600,  met/1e3,    m_weight);
        m_histFill->BookFillHist("jet1Eta", isEl,isCBA,24,   -3,3,   Jet1Eta,    m_weight);
        m_histFill->BookFillHist("jet2Eta", isEl,isCBA,24,   -3,3,   Jet2Eta,    m_weight);
        m_histFill->BookFillHist("Lep1Eta", isEl,isCBA,24,   -3,3,   Lep1Eta,    m_weight);
        m_histFill->BookFillHist("Lep2Eta", isEl,isCBA,24,   -3,3,   Lep2Eta,    m_weight);
        m_histFill->BookFillHist("fatjet1Eta", isEl,isCBA,20,   -2.5,2.5,   fatjet1Eta,    m_weight);
        m_histFill->BookFillHist("MFJj1", isEl,isCBA,40,   0,800,   MJj1/1e3,    m_weight);
        m_histFill->BookFillHist("MFJj2", isEl,isCBA,40,   0,800,   MJj2/1e3,    m_weight);
        m_histFill->BookFillHist("MFJj_maxDY", isEl,isCBA,40,   0,800,   MJj_maxDY/1e3,    m_weight);
        m_histFill->BookFillHist("mTWMax", isEl,isCBA,40,   0,3000,   MJj_maxDY/1e3,    m_weight);
        m_histFill->BookFillHist("DYFJj1", isEl,isCBA,40,   0,8,   DYJj1,    m_weight);
        m_histFill->BookFillHist("DYFJj2", isEl,isCBA,40,   0,8,   DYJj2,    m_weight);
        m_histFill->BookFillHist("DRFJj1", isEl,isCBA,40,   0,8,   DRJj1,    m_weight);
        m_histFill->BookFillHist("DRFJj2", isEl,isCBA,40,   0,8,   DRJj2,    m_weight);
        m_histFill->BookFillHist("DYFJ1FJ2", isEl,isCBA,40,   0,8,   DYJJ,    m_weight);
        m_histFill->BookFillHist("MFJ1FJ2", isEl,isCBA,40,   0,800,   MJJ/1e3,    m_weight);
        m_histFill->BookFillHist("Lep1Q", isEl,isCBA,4,   -2,2,   Lep1Q,    m_weight);
        m_histFill->BookFillHist("Lep2Q", isEl,isCBA,4,   -2,2,   Lep2Q,    m_weight);
        m_histFill->BookFillHist("dEtaJ1J2", isEl,isCBA,40,   0,2,   dEtaJ1J2,    m_weight);
        m_histFill->BookFillHist("fj1_SmoothWContained80_SF", isEl,isCBA,40,   0,3,   fj1_SmoothWContained80_SF,    m_weight);

        if(m_MJ && RunCR1) {
          m_histFill->SetDescription(description+"Inc");
          m_histFill->BookFillHist("Norm", isEl,isCBA,1,   0.5,1.5,   norma,    m_weight);
          m_histFill->BookFillHist("Njets", isEl,isCBA,11,   0,11,   Njets,    m_weight);
        m_histFill->BookFillHist("Nbjets", isEl,isCBA,11,   0,11,   Nbjets,    m_weight);
        m_histFill->BookFillHist("mll", isEl,isCBA,40,  0,2000,  Mll/1e3,    m_weight); //5GeV
          m_histFill->BookFillHist("Lep1Pt", isEl,isCBA,20,   0,600,   Lep1Pt/1e3,    m_weight);
        m_histFill->BookFillHist("Lep2Pt", isEl,isCBA,20,   0,400,   Lep2Pt/1e3,    m_weight);
          if(RunssWW || RunssWW500) m_histFill->BookFillHist("Mjj", isEl,isCBA,40,   0,1600,   Mjj/1e3,    m_weight);
        else m_histFill->BookFillHist("Mjj", isEl,isCBA,20,   0,200,   Mjj/1e3,    m_weight);
          m_histFill->BookFillHist("DYjj", isEl,isCBA,40,   0,8,   DYjj,    m_weight);
        m_histFill->BookFillHist("Meff", isEl,isCBA,nbins,   xMin,xMax,   Meff/1e3,    m_weight);
        m_histFill->BookFillHist("jet1Pt", isEl,isCBA,20,   0,800,   Jet1Pt/1e3,    m_weight);
        m_histFill->BookFillHist("jet2Pt", isEl,isCBA,20,   0,600,   Jet2Pt/1e3,    m_weight);
        m_histFill->BookFillHist("fatjet1Pt", isEl,isCBA,20,   0,1000,   fatjet1Pt/1e3,    m_weight);
        m_histFill->BookFillHist("fatjet1M", isEl,isCBA,20,   0,200,   fatjet1M/1e3,    m_weight);
          m_histFill->BookFillHist("MET", isEl,isCBA,20,  0,600,  met/1e3,    m_weight);
          m_histFill->BookFillHist("jet1Eta", isEl,isCBA,24,   -3,3,   Jet1Eta,    m_weight);
          m_histFill->BookFillHist("jet2Eta", isEl,isCBA,24,   -3,3,   Jet2Eta,    m_weight);
          m_histFill->BookFillHist("Lep1Eta", isEl,isCBA,24,   -3,3,   Lep1Eta,    m_weight);
          m_histFill->BookFillHist("Lep2Eta", isEl,isCBA,24,   -3,3,   Lep2Eta,    m_weight);
          m_histFill->BookFillHist("fatjet1Eta", isEl,isCBA,20,   -2.5,2.5,   fatjet1Eta,    m_weight);
          m_histFill->BookFillHist("MFJj1", isEl,isCBA,40,   0,800,   MJj1/1e3,    m_weight);
          m_histFill->BookFillHist("MFJj2", isEl,isCBA,40,   0,800,   MJj2/1e3,    m_weight);
          m_histFill->BookFillHist("MFJj_maxDY", isEl,isCBA,40,   0,800,   MJj_maxDY/1e3,    m_weight);
          m_histFill->BookFillHist("mTWMax", isEl,isCBA,40,   0,3000,   MJj_maxDY/1e3,    m_weight);
          m_histFill->BookFillHist("DYFJj1", isEl,isCBA,40,   0,8,   DYJj1,    m_weight);
          m_histFill->BookFillHist("DYFJj2", isEl,isCBA,40,   0,8,   DYJj2,    m_weight);
          m_histFill->BookFillHist("DRFJj1", isEl,isCBA,40,   0,8,   DRJj1,    m_weight);
          m_histFill->BookFillHist("DRFJj2", isEl,isCBA,40,   0,8,   DRJj2,    m_weight);
          m_histFill->BookFillHist("DYFJ1FJ2", isEl,isCBA,40,   0,8,   DYJJ,    m_weight);
          m_histFill->BookFillHist("MFJ1FJ2", isEl,isCBA,40,   0,800,   MJJ/1e3,    m_weight);
          m_histFill->BookFillHist("Lep1Q", isEl,isCBA,4,   -2,2,   Lep1Q,    m_weight);
          m_histFill->BookFillHist("Lep2Q", isEl,isCBA,4,   -2,2,   Lep2Q,    m_weight);
          m_histFill->BookFillHist("dEtaJ1J2", isEl,isCBA,40,   0,2,   dEtaJ1J2,    m_weight);
         }

        TString chargeTag;
   //     if(Lep1Flav==2 && Lep2Flav==2){
   //        if(Lep1Q==1 && Lep2Q==1) chargeTag = "pp"; else if(Lep1Q==-1 && Lep2Q==-1) chargeTag = "mm";
   //     }
   //     else if(Lep1Flav==2 && Lep2Flav==1) {
   //      if(Lep1Q==1) chargeTag = "pp"; else if(Lep1Q==-1) chargeTag = "mm";
   //     }
   //     else if(Lep1Flav==1 && Lep2Flav==2) {
   //     if(Lep2Q==1) chargeTag = "pp"; else if(Lep2Q==-1) chargeTag = "mm";
   //     }
   //     else if(Lep1Flav==1 && Lep2Flav==1) {
   //     if( (eff1 < eff2) || (eff1==0 && eff2==0) ) {if(Lep1Q==1) chargeTag = "pp"; else if(Lep1Q==-1) chargeTag = "mm";}
   //     else {if(Lep2Q==1) chargeTag = "pp"; else if(Lep2Q==-1) chargeTag = "mm";}
   //     }
   //     if(RunssWW) {
   //       m_histFill->SetDescription(description+chargeTag);
   //       m_histFill->BookFillHist("dPhill", isEl,isCBA,100,   0,3.15,   fabs(dPhill),    m_weight);
   //       m_histFill->BookFillHist("dEtall", isEl,isCBA,50,   0,5,   dEtall,    m_weight);
   //       m_histFill->BookFillHist("dRll", isEl,isCBA,60,   0,6,   dRll,    m_weight);
   //       m_histFill->BookFillHist("Meff", isEl,isCBA,nbins,   xMin,xMax,   Meff/1e3,    m_weight);
   //       m_histFill->BookFillHist("Njets", isEl,isCBA,11,   0,11,   Njets,    m_weight);
   //       m_histFill->BookFillHist("Nbjets", isEl,isCBA,11,   0,11,   Nbjets,    m_weight);
   //       m_histFill->BookFillHist("Nfatjets", isEl,isCBA,11,   0,11,   Nfatjets,    m_weight);
   //       m_histFill->BookFillHist("mll" , isEl,isCBA,40,  0,2000,  Mll/1e3,    m_weight);
   //       m_histFill->BookFillHist("Lep1Pt", isEl,isCBA,20,   0,600,   Lep1Pt/1e3,    m_weight);
   //       m_histFill->BookFillHist("Lep2Pt", isEl,isCBA,20,   0,400,   Lep2Pt/1e3,    m_weight);
   //       m_histFill->BookFillHist("jet1Pt", isEl,isCBA,20,   0,800,   Jet1Pt/1e3,    m_weight);
   //       m_histFill->BookFillHist("jet2Pt", isEl,isCBA,20,   0,600,   Jet2Pt/1e3,    m_weight);
   //       m_histFill->BookFillHist("fatjet1Pt", isEl,isCBA,20,   0,1000,   fatjet1Pt/1e3,    m_weight);
   //       m_histFill->BookFillHist("fatjet2Pt", isEl,isCBA,20,   0,1000,   fatjet2Pt/1e3,    m_weight);
   //       m_histFill->BookFillHist("fatjet1M", isEl,isCBA,20,   0,200,   fatjet1M/1e3,    m_weight);
   //       m_histFill->BookFillHist("fatjet2M", isEl,isCBA,20,   0,200,   fatjet2M/1e3,    m_weight);
   //       m_histFill->BookFillHist("MET", isEl,isCBA,20,  0,600,  met/1e3,    m_weight);
   //       m_histFill->BookFillHist("Mjj", isEl,isCBA,40,   0,1600,   Mjj/1e3,    m_weight);
   //       m_histFill->BookFillHist("DYjj", isEl,isCBA,40,   0,8,   DYjj,    m_weight);

   //       m_histFill->BookFillHist("MFJj1", isEl,isCBA,40,   0,800,   MJj1/1e3,    m_weight);
   //       m_histFill->BookFillHist("MFJj2", isEl,isCBA,40,   0,800,   MJj2/1e3,    m_weight);
   //       m_histFill->BookFillHist("MFJj_maxDY", isEl,isCBA,40,   0,800,   MJj_maxDY/1e3,    m_weight);
   //       m_histFill->BookFillHist("DYFJj1", isEl,isCBA,40,   0,8,   DYJj1,    m_weight);
   //       m_histFill->BookFillHist("DYFJj2", isEl,isCBA,40,   0,8,   DYJj2,    m_weight);
   //       m_histFill->BookFillHist("DRFJj1", isEl,isCBA,40,   0,8,   DRJj1,    m_weight);
   //       m_histFill->BookFillHist("DRFJj2", isEl,isCBA,40,   0,8,   DRJj2,    m_weight);
   //       m_histFill->BookFillHist("DYFJ1FJ2", isEl,isCBA,40,   0,8,   DYJJ,    m_weight);
   //       m_histFill->BookFillHist("MFJ1FJ2", isEl,isCBA,40,   0,800,   MJJ/1e3,    m_weight);

   //       m_histFill->BookFillHist("jet1Eta", isEl,isCBA,24,   -3,3,   Jet1Eta,    m_weight);
   //       m_histFill->BookFillHist("jet2Eta", isEl,isCBA,24,   -3,3,   Jet2Eta,    m_weight);
   //       m_histFill->BookFillHist("Lep1Eta", isEl,isCBA,24,   -3,3,   Lep1Eta,    m_weight);
   //       m_histFill->BookFillHist("Lep2Eta", isEl,isCBA,24,   -3,3,   Lep2Eta,    m_weight);
   //       m_histFill->BookFillHist("fatjet1Eta", isEl,isCBA,20,   -2.5,2.5,   fatjet1Eta,    m_weight);
   //       m_histFill->BookFillHist("Lep1Q", isEl,isCBA,4,   -2,2,   Lep1Q,    m_weight);
   //       m_histFill->BookFillHist("Lep2Q", isEl,isCBA,4,   -2,2,   Lep2Q,    m_weight);
   //     }

	// can do better but for now I just call the BookFillHist twice to get the inclusive and separated histograms
	TString flav;
	if(Lep1Flav==1 && Lep2Flav==1) flav = "ee";
	else if(Lep1Flav==2 && Lep2Flav==2) flav = "mumu";
	else if(Lep1Flav==1 && Lep2Flav==2) flav = "emu";
	else if(Lep1Flav==2 && Lep2Flav==1) flav = "mue";
         m_histFill->SetDescription(s_NP_lep+description+flav);
	m_histFill->BookFillHist("Meff", isEl,isCBA,nbins,   xMin,xMax,   Meff/1e3,    m_weight);
        m_histFill->BookFillHist("Njets", isEl,isCBA,11,   0,11,   Njets,    m_weight);
        m_histFill->BookFillHist("Nbjets", isEl,isCBA,11,   0,11,   Nbjets,    m_weight);
        m_histFill->BookFillHist("mll" , isEl,isCBA,40,  0,2000,  Mll/1e3,    m_weight);
        m_histFill->BookFillHist("Lep1Pt", isEl,isCBA,20,   0,600,   Lep1Pt/1e3,    m_weight);
        m_histFill->BookFillHist("Lep2Pt", isEl,isCBA,20,   0,400,   Lep2Pt/1e3,    m_weight);
        m_histFill->BookFillHist("jet1Pt", isEl,isCBA,20,   0,800,   Jet1Pt/1e3,    m_weight);
        m_histFill->BookFillHist("jet2Pt", isEl,isCBA,20,   0,600,   Jet2Pt/1e3,    m_weight);
        m_histFill->BookFillHist("fatjet1Pt", isEl,isCBA,20,   0,1000,   fatjet1Pt/1e3,    m_weight);
        m_histFill->BookFillHist("fatjet1M", isEl,isCBA,20,   0,200,   fatjet1M/1e3,    m_weight);
        m_histFill->BookFillHist("MET", isEl,isCBA,20,  0,600,  met/1e3,    m_weight);
        m_histFill->BookFillHist("jet1Eta", isEl,isCBA,24,   -3,3,   Jet1Eta,    m_weight);
        m_histFill->BookFillHist("jet2Eta", isEl,isCBA,24,   -3,3,   Jet2Eta,    m_weight);
        m_histFill->BookFillHist("Lep1Eta", isEl,isCBA,24,   -3,3,   Lep1Eta,    m_weight);
        m_histFill->BookFillHist("Lep2Eta", isEl,isCBA,24,   -3,3,   Lep2Eta,    m_weight);
        m_histFill->BookFillHist("fatjet1Eta", isEl,isCBA,20,   -2.5,2.5,   fatjet1Eta,    m_weight);
        if(RunssWW || RunssWW500) m_histFill->BookFillHist("Mjj", isEl,isCBA,40,   0,1600,   Mjj/1e3,    m_weight);
        else m_histFill->BookFillHist("Mjj", isEl,isCBA,20,   0,200,   Mjj/1e3,    m_weight);
        m_histFill->BookFillHist("DYjj", isEl,isCBA,40,   0,8,   DYjj,    m_weight);
        m_histFill->BookFillHist("MFJj1", isEl,isCBA,40,   0,800,   MJj1/1e3,    m_weight);
        m_histFill->BookFillHist("MFJj2", isEl,isCBA,40,   0,800,   MJj2/1e3,    m_weight);
        m_histFill->BookFillHist("MFJj_maxDY", isEl,isCBA,40,   0,800,   MJj_maxDY/1e3,    m_weight);
        m_histFill->BookFillHist("DYFJj1", isEl,isCBA,40,   0,8,   DYJj1,    m_weight);
        m_histFill->BookFillHist("DYFJj2", isEl,isCBA,40,   0,8,   DYJj2,    m_weight);
        m_histFill->BookFillHist("DRFJj1", isEl,isCBA,40,   0,8,   DRJj1,    m_weight);
        m_histFill->BookFillHist("DRFJj2", isEl,isCBA,40,   0,8,   DRJj2,    m_weight);
        m_histFill->BookFillHist("DYFJ1FJ2", isEl,isCBA,40,   0,8,   DYJJ,    m_weight);
        m_histFill->BookFillHist("MFJ1FJ2", isEl,isCBA,40,   0,800,   MJJ/1e3,    m_weight);
        m_histFill->BookFillHist("Lep1Q", isEl,isCBA,4,   -2,2,   Lep1Q,    m_weight);
        m_histFill->BookFillHist("Lep2Q", isEl,isCBA,4,   -2,2,   Lep2Q,    m_weight);
     
   //     if(RunssWW) {
   //       m_histFill->SetDescription(description+flav+chargeTag);
   //       m_histFill->BookFillHist("dPhill", isEl,isCBA,100,   0,3.15,   fabs(dPhill),    m_weight);
   //       m_histFill->BookFillHist("dEtall", isEl,isCBA,50,   0,5,   dEtall,    m_weight);
   //       m_histFill->BookFillHist("dRll", isEl,isCBA,60,   0,6,   dRll,    m_weight);
   //       m_histFill->BookFillHist("Meff", isEl,isCBA,nbins,   xMin,xMax,   Meff/1e3,    m_weight);
   //       m_histFill->BookFillHist("Njets", isEl,isCBA,11,   0,11,   Njets,    m_weight);
   //       m_histFill->BookFillHist("Nbjets", isEl,isCBA,11,   0,11,   Nbjets,    m_weight);
   //       m_histFill->BookFillHist("Nfatjets", isEl,isCBA,11,   0,11,   Nfatjets,    m_weight);
   //       m_histFill->BookFillHist("mll" , isEl,isCBA,40,  0,2000,  Mll/1e3,    m_weight);
   //       m_histFill->BookFillHist("Lep1Pt", isEl,isCBA,20,   0,600,   Lep1Pt/1e3,    m_weight);
   //       m_histFill->BookFillHist("Lep2Pt", isEl,isCBA,20,   0,400,   Lep2Pt/1e3,    m_weight);
   //       m_histFill->BookFillHist("jet1Pt", isEl,isCBA,20,   0,800,   Jet1Pt/1e3,    m_weight);
   //       m_histFill->BookFillHist("jet2Pt", isEl,isCBA,20,   0,600,   Jet2Pt/1e3,    m_weight);
   //       m_histFill->BookFillHist("fatjet1Pt", isEl,isCBA,20,   0,1000,   fatjet1Pt/1e3,    m_weight);
   //       m_histFill->BookFillHist("fatjet2Pt", isEl,isCBA,20,   0,1000,   fatjet2Pt/1e3,    m_weight);
   //       m_histFill->BookFillHist("fatjet1M", isEl,isCBA,20,   0,200,   fatjet1M/1e3,    m_weight);
   //       m_histFill->BookFillHist("fatjet2M", isEl,isCBA,20,   0,200,   fatjet2M/1e3,    m_weight);
   //       m_histFill->BookFillHist("MET", isEl,isCBA,20,  0,600,  met/1e3,    m_weight);
   //       m_histFill->BookFillHist("Mjj", isEl,isCBA,40,   0,1600,   Mjj/1e3,    m_weight);
   //       m_histFill->BookFillHist("DYjj", isEl,isCBA,40,   0,8,   DYjj,    m_weight);

   //       m_histFill->BookFillHist("MFJj1", isEl,isCBA,40,   0,800,   MJj1/1e3,    m_weight);
   //       m_histFill->BookFillHist("MFJj2", isEl,isCBA,40,   0,800,   MJj2/1e3,    m_weight);
   //       m_histFill->BookFillHist("MFJj_maxDY", isEl,isCBA,40,   0,800,   MJj_maxDY/1e3,    m_weight);
   //       m_histFill->BookFillHist("DYFJj1", isEl,isCBA,40,   0,8,   DYJj1,    m_weight);
   //       m_histFill->BookFillHist("DYFJj2", isEl,isCBA,40,   0,8,   DYJj2,    m_weight);
   //       m_histFill->BookFillHist("DRFJj1", isEl,isCBA,40,   0,8,   DRJj1,    m_weight);
   //       m_histFill->BookFillHist("DRFJj2", isEl,isCBA,40,   0,8,   DRJj2,    m_weight);
   //       m_histFill->BookFillHist("DYFJ1FJ2", isEl,isCBA,40,   0,8,   DYJJ,    m_weight);
   //       m_histFill->BookFillHist("MFJ1FJ2", isEl,isCBA,40,   0,800,   MJJ/1e3,    m_weight);

   //       m_histFill->BookFillHist("jet1Eta", isEl,isCBA,24,   -3,3,   Jet1Eta,    m_weight);
   //       m_histFill->BookFillHist("jet2Eta", isEl,isCBA,24,   -3,3,   Jet2Eta,    m_weight);
   //       m_histFill->BookFillHist("Lep1Eta", isEl,isCBA,24,   -3,3,   Lep1Eta,    m_weight);
   //       m_histFill->BookFillHist("Lep2Eta", isEl,isCBA,24,   -3,3,   Lep2Eta,    m_weight);
   //       m_histFill->BookFillHist("fatjet1Eta", isEl,isCBA,20,   -2.5,2.5,   fatjet1Eta,    m_weight);
   //       m_histFill->BookFillHist("Lep1Q", isEl,isCBA,4,   -2,2,   Lep1Q,    m_weight);
   //       m_histFill->BookFillHist("Lep2Q", isEl,isCBA,4,   -2,2,   Lep2Q,    m_weight);
   //     }
  }
}


