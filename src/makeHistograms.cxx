#include "generateHistogram.h"
#include "generateHistogram_2lep.h"
#include "generateHistogram_3lep.h"
#include "generateHistogram_WW.h"
#include "ReadTruthTree.h"
#include <vector>
#include <iostream>
#include <TSystem.h>

// This macro takes ntuples produced using the CxAODFramework framework as inputs (with a very loose event selection)
// The macro will then apply:
// * The event selection of your chosen analysis
// * fill the histograms
// * 

int main( int argc, char * argv[]  ){

  int sampleToRun = 0;
  if(argc > 1) sampleToRun = (std::atoi(argv[1]));
  TString outputPath = "./out/";
  if(argc > 2) outputPath = argv[2];
  const std::vector<TString> DSIDs = {"data15","data16","data17","data18","GHH600X","ttbar","singletopt","singletops","singletopWt","Wjets","Zjets","WWW","VVV","Wgamma","Zgamma","ssWW","ttW","ttZ","tz","WZ","ZZ","GHH300X","GHH300Y","GHH600Y","GHH900X", "GHH3f600f0", "GHH3fm600f0","GHH3f350f2100","GHH3fm350f2100","GHH3fm350fm2100","GHH3f350fm2100","GHH3f0f3000","GHH3f0fm3000", "GHH6f650f0","GHH6fm650f0","GHH6f400f2400","GHH6fm400f2400","GHH6fm400fm2400","GHH6f400fm2400","GHH6f0f3500","GHH6f0fm3500", "GHH9f800f0","GHH9fm800f0","GHH9f600f3600","GHH9fm600f3600","GHH9fm600fm3600","GHH9f600fm3600","GHH9f0f5000","GHH9f0fm5000","GHH300fW1230fWW0", "GHH300fW770fWW0", "GHH300fW1910fWW0", "GHH300fW660fWW3960", "GHH300fW415fWW2490", "GHH300fW1015fWW6090", "GHH300fW0fWW4600", "GHH300fW0fWW2450", "GHH300fW0fWW7100", "GHH300fWm730fWW4380", "GHH300fWm455fWW2730", "GHH300fWm1125fWW6750", "GHH300fWm1360fWW0", "GHH300fWm810fWW0", "GHH300fWm2180fWW0", "GHH300fWm610fWWm3660", "GHH300fWm370fWWm2220", "GHH300fWm960fWWm5760", "GHH300fW0fWWm4650", "GHH300fW0fWWm2300", "GHH300fW0fWWm7250", "GHH300fW710fWWm4260", "GHH300fW430fWWm2580", "GHH300fW1110fWWm6660", "GHH300fW1000fWW2000", "GHH300fW500fWW1000", "GHH300fW1500fWW3000", "GHH300fW300fWW4500", "GHH300fW150fWW2250", "GHH300fW470fWW7050", "GHH300fWm330fWW4950", "GHH300fWm150fWW2250", "GHH300fWm490fWW7350", "GHH300fWm1200fWW2400", "GHH300fWm500fWW1000", "GHH300fWm1950fWW3900", "GHH300fWm1100fWWm2200", "GHH300fWm500fWWm1000", "GHH300fWm1800fWWm3600", "GHH300fWm300fWWm4500", "GHH300fWm140fWWm2100", "GHH300fWm470fWWm7050", "GHH300fW330fWWm4950", "GHH300fW150fWWm2250", "GHH300fW500fWWm7500", "GHH300fW1100fWWm2200", "GHH300fW500fWWm1000", "GHH300fW1800fWWm3600", "GHH600fW1350fWW0", "GHH600fW820fWW0", "GHH600fW2130fWW0", "GHH600fW765fWW4590", "GHH600fW470fWW2820", "GHH600fW1205fWW7230", "GHH600fW0fWW6200", "GHH600fW0fWW3800", "GHH600fW0fWW9750", "GHH600fWm855fWW5130", "GHH600fWm520fWW3120", "GHH600fWm1355fWW8130", "GHH600fWm1340fWW0", "GHH600fWm790fWW0", "GHH600fWm2130fWW0", "GHH600fWm755fWWm4530", "GHH600fWm455fWWm2730", "GHH600fWm1195fWWm7170", "GHH600fW0fWWm6250", "GHH600fW0fWWm3800", "GHH600fW0fWWm9900", "GHH600fW915fWWm5490", "GHH600fW555fWWm3330", "GHH600fW1440fWWm8640", "GHH600fW1200fWW2400", "GHH600fW500fWW1000", "GHH600fW1900fWW3800", "GHH600fW400fWW6000", "GHH600fW200fWW3000", "GHH600fW650fWW9750", "GHH600fWm400fWW6000", "GHH600fWm200fWW3000", "GHH600fWm650fWW9750", "GHH600fWm1250fWW2500", "GHH600fWm500fWW1000", "GHH600fWm2000fWW4000", "GHH600fWm1200fWWm2400", "GHH600fWm500fWWm1000", "GHH600fWm1900fWWm3800", "GHH600fWm400fWWm6000", "GHH600fWm190fWWm2850", "GHH600fWm650fWWm9750", "GHH600fW450fWWm6750", "GHH600fW200fWWm3000", "GHH600fW670fWWm10050", "GHH600fW1300fWWm2600", "GHH600fW600fWWm1200", "GHH600fW2050fWWm4100", "GHH900fW1570fWW0", "GHH900fW940fWW0", "GHH900fW2510fWW0", "GHH900fW1015fWW6090", "GHH900fW610fWW3660", "GHH900fW1615fWW9690", "GHH900fW0fWW8800", "GHH900fW0fWW5250", "GHH900fW0fWW14100", "GHH900fWm1165fWW6990", "GHH900fWm695fWW4170", "GHH900fWm1865fWW11190", "GHH900fWm1550fWW0", "GHH900fWm920fWW0", "GHH900fWm2480fWW0", "GHH900fWm1000fWWm6000", "GHH900fWm495fWWm2970", "GHH900fWm1605fWWm9630", "GHH900fW0fWWm8700", "GHH900fW0fWWm5150", "GHH900fW0fWWm13950", "GHH900fW1205fWWm7230", "GHH900fW720fWWm4320", "GHH900fW1925fWWm11550", "GHH900fW1400fWW2800", "GHH900fW700fWW1400", "GHH900fW2300fWW4600", "GHH900fW550fWW8250", "GHH900fW300fWW4500", "GHH900fW900fWW13500", "GHH900fWm600fWW9000", "GHH900fWm300fWW4500", "GHH900fWm1000fWW15000", "GHH900fWm1500fWW3000", "GHH900fWm800fWW1600", "GHH900fWm2400fWW4800", "GHH900fWm1400fWWm2800", "GHH900fWm700fWWm1400", "GHH900fWm2300fWWm4600", "GHH900fWm550fWWm8250", "GHH900fWm250fWWm3750", "GHH900fWm850fWWm12750", "GHH900fW600fWWm9000", "GHH900fW300fWWm4500", "GHH900fW1000fWWm15000", "GHH900fW1500fWWm3000", "GHH900fW800fWWm1600", "GHH900fW2400fWWm4800", "GHH300fW1350fWW0", "GHH360fW1350fWW0", "GHH420fW1350fWW0", "GHH480fW1350fWW0", "GHH540fW1350fWW0", "GHH660fW1350fWW0", "GHH720fW1350fWW0", "GHH780fW1350fWW0", "GHH840fW1350fWW0", "GHH900fW1350fWW0", "GHH300fW0fWW6200", "GHH360fW0fWW6200", "GHH420fW0fWW6200", "GHH480fW0fWW6200", "GHH540fW0fWW6200", "GHH660fW0fWW6200", "GHH720fW0fWW6200", "GHH780fW0fWW6200", "GHH840fW0fWW6200", "GHH900fW0fWW6200", "GHH1000fW1350fWW0", "GHH1200fW1350fWW0", "GHH1500fW1350fWW0", "GHH2000fW1350fWW0", "GHH1000fW0fWW6200", "GHH1200fW0fWW6200", "GHH1500fW0fWW6200", "GHH2000fW0fWW6200", "WZ_a","WZ_d","WZ_e","WZAlternative","ssWWnew","ssWWNewAlternative"};
  TString NAME = DSIDs.at(sampleToRun);
//  outputPath = outputPath;
  system("mkdir "+outputPath);
  TString region = "SR"; //"VR", "ssWW", "WZ"
  TString regime = "2Lep_";
  TString prev_file = outputPath + "output_"+regime + DSIDs.at(sampleToRun) +".root";
  system("rm "+prev_file);

  generateHistogram * config = nullptr;

  if (region == "WZ") config = new generateHistogram_3lep(); // or run 3L;
  else {
    config = new generateHistogram_2lep(); //2L;
    if (region == "ssWW") config->runssWW(true);
    if (region == "VR") config->runSideBand(true);
  }
  //generateHistogram_WW config; //ssWW CR, WWW CR;
  TString inputPath = "/afs/cern.ch/user/g/grbarbou/eos/HeavyHiggsAnalysis/samples/newsignal/";
//  TString inputPath = "/home/storage2/xuyue/atlas/GHH/Mini-ntuple/ntuple_GHH_r03-01_2smallRjetsor1largeRjet/MCade/reduce2L/";
  //TString inputPath = "/home/storage2/xuyue/atlas/GHH/Mini-ntuple/ntuple_GHH_r03-01_2smallRjetsor1largeRjet/MCade/reduce3L/";
  config->SetInputPath(inputPath);

  //TString outputPath2 = outputPath+"/"+DSIDs.at(sampleToRun);
  //gSystem->Exec(("rm -rf "+outputPath2).Data());
  //gSystem->Exec(("mkdir -p "+outputPath2).Data());
  //or OS 3L samples  
  config->SetOutputPath(outputPath); //Set the output path for histograms
  config->SetNEvents(-1);  // Number of events to run over, -1 = all events
  config->SetDebug(false);  // Run in debug mode
  config->SetLumi(139);  // Set the luminosity to normalise MC to, in fb
  config->RunMJ(false);//non-prompt,  Yanhui
  config->RunNonPrompt(false);//for non-prompt, CR1, Yanhui
  config->RunVgamma(false); // for data driven Vgamma
  config->RunCBA(false);//Yanhui
  config->RunMC16a(false);//Yanhui
  config->UseSFs(false);//Yanhui
  config->TightIso(false);//Yanhui
  //Do you aply the tau veto?!!!

  bool doSystematics = false;

  if (doSystematics) {
    //Extract list of systematics tree names from the target root file
    //In future, maybe just save this list to csv? As it should not change
    std::vector <TString> sysTrees;
    TFile *myfile = TFile::Open(inputPath + DSIDs.at(sampleToRun) + ".root");
    for (TObject *i : *myfile->GetListOfKeys()) {
      sysTrees.push_back(i->GetName());
    }
    myfile->Close();

    for (TString sysTree : sysTrees) {
      config->GenerateHistogram(generateHistogram::TwoLepton, DSIDs.at(sampleToRun), sysTree); //2L
      //config->GenerateHistogram(generateHistogram::ThreeLepton,DSIDs.at(sampleToRun), sysTree); //or run 3L
      //Is above needed? Only difference using generateHistogram::ThreeLepton is the naming of output file
    }
  }
  else {
    config->GenerateHistogram(generateHistogram::TwoLepton, DSIDs.at(sampleToRun), "Nominal"); //2L
    //config->GenerateHistogram(generateHistogram::ThreeLepton,DSIDs.at(sampleToRun), "Nominal"); //or run 3L
  }
//  delete config; //I know this is good practice, but not really needed
}

