#include "histSvc.h"
#include <TFile.h>
#include <TH1.h>
#include <TH2.h>
#include <TKey.h>
#include <TList.h>
#include <TClass.h>
#include <TRegexp.h>
#include "TChain.h"
#include "TLorentzVector.h"
#include "generateHistogram.h"

#include <iostream>
#include <fstream>
#include <vector>
#include "TRandom3.h"

histSvc::histSvc( TString outputPath ):
  m_outputPath("./"),
  m_DSID(),
  m_flav_1(-1),
  m_flav_2(-1),
  m_njets(-1),
  m_nfjets(-1),
  m_ntags(-1),
  m_ptv(-1),
  m_flavor(""),
  m_description("")
{
  m_outputPath = outputPath;
  m_histograms.clear();
  m_histograms_2d.clear();
  m_histoMap.clear();
}

histSvc::~histSvc() {

}

void histSvc::SetAnalysisType( generateHistogram::AnalysisType type ) {

  m_type = type;

}

void histSvc::SetProcess( TString DSID, generateHistogram::Process process ){

  m_DSID = DSID;
  m_process = process;

}

void histSvc::SetEventFlav( int flav_1, int flav_2 ){

  m_flav_1 = flav_1;
  m_flav_2 = flav_2;

}

void histSvc::SetNjets( int njets ){

  m_njets = njets;

}
void histSvc::SetNFjets( int njets ){

   m_nfjets = njets;

}

void histSvc::SetpTV( float ptv ){

  m_ptv = ptv;

}

void histSvc::SetFlavor( int j1Flav,int j2Flav) {

  if       (j1Flav < 0) return;
  if       (j1Flav == 5 && j2Flav < 0) m_flavor = "b";
  else if  (j1Flav == 4 && j2Flav < 0) m_flavor = "c";
  else if  (j1Flav == 5 && j2Flav == 5)  m_flavor = "bb";
  else if ((j1Flav == 5 || j2Flav == 5)
        && (j1Flav == 4 || j2Flav == 4)) m_flavor = "bc";
  else if  (j1Flav == 5 || j2Flav == 5)  m_flavor = "bl";
  else if  (j1Flav == 4 && j2Flav == 4)  m_flavor = "cc";
  else if  (j1Flav == 4 || j2Flav == 4)  m_flavor = "cl";
  else m_flavor = "l";

}
void histSvc::SetNtags( int ntags ){

  m_ntags = ntags;

}


void histSvc::SetDescription( TString description){

  m_description = description;

}

void histSvc::SetSystematicTree( TString sysTree){

  m_sysTree = sysTree;

}

void histSvc::BookFillHist( TString varName, bool isEl,bool isCBA,int nBins, float xmin, float xmax, float value, float weight) {

   TString histName = GetHistoName(varName,isEl,isCBA);
   auto t = m_histoMap.find(histName); 
    if (t == m_histoMap.end()){
	m_histoMap[histName] = m_histograms.size();
	m_histograms.push_back(new TH1F(histName,histName, nBins, xmin, xmax ));
    m_histograms.back()->GetXaxis()->SetTitle(varName);
	}
   m_histograms.at(m_histoMap[GetHistoName(varName,isEl,isCBA)])->Fill(value,weight);
}

void histSvc::BookFillHist( TString varName, bool isEl,bool isCBA,int nBinsX, float xmin, float xmax, float xvalue, int nBinsY, float ymin, float ymax, float yvalue, float weight) {
  
   TString histName = GetHistoName(varName, isEl,isCBA);
   auto t = m_histoMap.find(histName);
    if (t == m_histoMap.end()){
    m_histoMap[histName] = m_histograms_2d.size();
    m_histograms_2d.push_back(new TH2F(histName,histName, nBinsX, xmin, xmax, nBinsY, ymin, ymax));
    m_histograms_2d.back()->GetXaxis()->SetTitle(varName);
    }
   m_histograms_2d.at(m_histoMap[GetHistoName(varName, isEl,isCBA)])->Fill(xvalue,yvalue,weight);
}  





void histSvc::FillCutflow( int bin, float weight, TString name ){

  if(name == "Unweighted")  m_cutflow_Unweighted->Fill(bin,weight);
  else if(name == "EventWeight")  m_cutflow_EventWeight->Fill(bin,weight);
  else if(name == "FullWeight")  m_cutflow_FullWeight->Fill(bin,weight);

}

void histSvc::WriteHists() {

  TString outputFileName = m_outputPath;
  outputFileName += "/output_";
  if( m_type == generateHistogram::OneLepton ) outputFileName += "1Lep_";
  else if( m_type == generateHistogram::TwoLepton ) outputFileName += "2Lep_";
  else if( m_type == generateHistogram::ThreeLepton ) outputFileName += "3Lep_";
  outputFileName += m_DSID;
//  outputFileName += "_"+m_sysTree;
  outputFileName += ".root";
  TFile* f = new TFile(outputFileName,"update"); //"RECREATE");
  TString histName;
  for( unsigned int i=0;i<m_histograms.size(); ++i ){ 
    histName = m_histograms.at(i)->GetName();
    m_histograms.at(i)->Write();//Yanhui
	delete m_histograms.at(i);
  }
  for( unsigned int ii=0;ii<m_histograms_2d.size(); ++ii ){
    histName = m_histograms_2d.at(ii)->GetName();
    m_histograms_2d.at(ii)->Write();//Yanhui
    delete m_histograms_2d.at(ii);
  }
  f->Close();  
  delete f;
}

TString histSvc::TruthFlavLabel(){

  // std::cout << "Inside TruthFlavLabel" << std::endl;
  // std::cout << m_flav_1 << " " << m_flav_2 << std::endl;
  // std::cout << m_flav_1+ m_flav_2 << std::endl;
  if(m_flav_1+ m_flav_2 == 10) return "bb";
  else if(m_flav_1+ m_flav_2 == 9) return "bc";
  else if(m_flav_1+ m_flav_2 == 5) return "bl";
  else if(m_flav_1+ m_flav_2 == 8) return "cc";
  else if(m_flav_1+ m_flav_2 == 4) return "cl";
  else if(m_flav_1+ m_flav_2 == 0) return "ll";
  else {
    std::cout << "Could not find truth flavour matching for " << m_flav_1+ m_flav_2 << " - exiting" << std::endl;
    exit(0);
  } 
}


TString histSvc::GetHistoName(TString varName, bool isEl, bool isCBA){

  TString histogramName = "h";

  //if( m_type==generateHistogram::ZeroLepton ) histogramName += "0Lep";
  //else if( m_type==generateHistogram::OneLepton ) histogramName += "1Lep";
  //else if( m_type==generateHistogram::TwoLepton ) histogramName += "2Lep";
  TString SampleName = "";
  //histogramName += "_";
  if( m_process==generateHistogram::photonconversion){
     SampleName = "PhotonConversion";
  }
  if( m_process==generateHistogram::nonprompt ){
      SampleName = "NonPrompt";
  }
  
  if( m_process==generateHistogram::GHH300X ){
    SampleName = "GHH300X";
  }
  if( m_process==generateHistogram::GHH300Y ){
    SampleName = "GHH300Y";
  }
  if( m_process==generateHistogram::GHH600X ){
    SampleName = "GHH600X";
  }
  if( m_process==generateHistogram::GHH600Y ){
    SampleName = "GHH600Y";
  }
  if( m_process==generateHistogram::GHH900X ){
    SampleName = "GHH900X";
  }  
  if( m_process==generateHistogram::Wjets ){
    SampleName = "Wjets";//Yanhui
    //SampleName = "NonPrompt"; // 3lCR 
    //SampleName = m_flavor;
  }
  if( m_process==generateHistogram::Zjets ){
    SampleName = "Zjets";//Yanhui
    //SampleName = "ChargeFlip";//Yanhui
    //SampleName = m_flavor;
  }
  if( m_process==generateHistogram::ttbar ){
    SampleName = "ttbar";
    //SampleName = "NonPrompt"; // 3lCR
	//SampleName = m_flavor;//Yanhui temp
  }
  if( m_process==generateHistogram::stop ){
    SampleName = "SingleTop";
    //SampleName = "NonPrompt"; // 3lCR
  }
  if( m_process==generateHistogram::Vgamma ){
    SampleName = "Vgamma";
    //SampleName = "PhotonConversion"; // 3lCR
  }
  if( m_process==generateHistogram::ssWW ){
    SampleName = "ssWW";
  }
  if( m_process==generateHistogram::WZ ){
    SampleName = "WZ";
  }
  if( m_process==generateHistogram::ZZ ){
    SampleName = "ZZ";
  }
//  if( m_process==generateHistogram::ttW ){
//    SampleName = "ttW";
//  }
//  if( m_process==generateHistogram::ttZ ){
//    SampleName = "ttZ";
//  }
//  if( m_process==generateHistogram::tZ ){
//    SampleName = "tZ";
//  }
  if( m_process==generateHistogram::topX ){
    SampleName = "TopX";
  }
  //if( m_process==generateHistogram::Wgamma ){
  //  SampleName = "Wgamma";
  //}
  //if( m_process==generateHistogram::Zgamma ){
  //  SampleName = "Zgamma";
  //}
  if( m_process==generateHistogram::WWW ){
    SampleName = "WWW";
  }
  if( m_process==generateHistogram::VVV ){
    SampleName = "VVV";
  }
  if( m_process==generateHistogram::data ){
    SampleName = "data"; //when run RunOS=true, change to "ChargeFlip"
    //SampleName = "ChargeFlip";
  }
  //second signal
if( m_process==generateHistogram::GHH3f600f0 ){ SampleName = "GHH3f600f0";}
if( m_process==generateHistogram::GHH3fm600f0 ){ SampleName = "GHH3fm600f0";}
if( m_process==generateHistogram::GHH3f350f2100 ){ SampleName = "GHH3f350f2100";}
if( m_process==generateHistogram::GHH3fm350f2100 ){ SampleName = "GHH3fm350f2100";}
if( m_process==generateHistogram::GHH3fm350fm2100 ){ SampleName = "GHH3fm350fm2100";}
if( m_process==generateHistogram::GHH3f350fm2100 ){ SampleName = "GHH3f350fm2100";}
if( m_process==generateHistogram::GHH3f0f3000 ){ SampleName = "GHH3f0f3000";}
if( m_process==generateHistogram::GHH3f0fm3000 ){ SampleName = "GHH3f0fm3000";}
if( m_process==generateHistogram::GHH6f650f0 ){ SampleName = "GHH6f650f0";}
if( m_process==generateHistogram::GHH6fm650f0 ){ SampleName = "GHH6fm650f0";}
if( m_process==generateHistogram::GHH6f400f2400 ){ SampleName = "GHH6f400f2400";}
if( m_process==generateHistogram::GHH6fm400f2400 ){ SampleName = "GHH6fm400f2400";}
if( m_process==generateHistogram::GHH6fm400fm2400 ){ SampleName = "GHH6fm400fm2400";}
if( m_process==generateHistogram::GHH6f400fm2400 ){ SampleName = "GHH6f400fm2400";}
if( m_process==generateHistogram::GHH6f0f3500 ){ SampleName = "GHH6f0f3500";}
if( m_process==generateHistogram::GHH6f0fm3500 ){ SampleName = "GHH6f0fm3500";}
if( m_process==generateHistogram::GHH9f800f0 ){ SampleName = "GHH9f800f0";}
if( m_process==generateHistogram::GHH9fm800f0 ){ SampleName = "GHH9fm800f0";}
if( m_process==generateHistogram::GHH9f600f3600 ){ SampleName = "GHH9f600f3600";}
if( m_process==generateHistogram::GHH9fm600f3600 ){ SampleName = "GHH9fm600f3600";}
if( m_process==generateHistogram::GHH9fm600fm3600 ){ SampleName = "GHH9fm600fm3600";}
if( m_process==generateHistogram::GHH9f600fm3600 ){ SampleName = "GHH9f600fm3600";}
if( m_process==generateHistogram::GHH9f0f5000 ){ SampleName = "GHH9f0f5000";}
if( m_process==generateHistogram::GHH9f0fm5000 ){ SampleName = "GHH9f0fm5000";}
if( m_process==generateHistogram::GHH300fW1230fWW0 ){ SampleName = "GHH300fW1230fWW0";}
if( m_process==generateHistogram::GHH300fW770fWW0 ){ SampleName = "GHH300fW770fWW0";}
if( m_process==generateHistogram::GHH300fW1910fWW0 ){ SampleName = "GHH300fW1910fWW0";}
if( m_process==generateHistogram::GHH300fW660fWW3960 ){ SampleName = "GHH300fW660fWW3960";}
if( m_process==generateHistogram::GHH300fW415fWW2490 ){ SampleName = "GHH300fW415fWW2490";}
if( m_process==generateHistogram::GHH300fW1015fWW6090 ){ SampleName = "GHH300fW1015fWW6090";}
if( m_process==generateHistogram::GHH300fW0fWW4600 ){ SampleName = "GHH300fW0fWW4600";}
if( m_process==generateHistogram::GHH300fW0fWW2450 ){ SampleName = "GHH300fW0fWW2450";}
if( m_process==generateHistogram::GHH300fW0fWW7100 ){ SampleName = "GHH300fW0fWW7100";}
if( m_process==generateHistogram::GHH300fWm730fWW4380 ){ SampleName = "GHH300fWm730fWW4380";}
if( m_process==generateHistogram::GHH300fWm455fWW2730 ){ SampleName = "GHH300fWm455fWW2730";}
if( m_process==generateHistogram::GHH300fWm1125fWW6750 ){ SampleName = "GHH300fWm1125fWW6750";}
if( m_process==generateHistogram::GHH300fWm1360fWW0 ){ SampleName = "GHH300fWm1360fWW0";}
if( m_process==generateHistogram::GHH300fWm810fWW0 ){ SampleName = "GHH300fWm810fWW0";}
if( m_process==generateHistogram::GHH300fWm2180fWW0 ){ SampleName = "GHH300fWm2180fWW0";}
if( m_process==generateHistogram::GHH300fWm610fWWm3660 ){ SampleName = "GHH300fWm610fWWm3660";}
if( m_process==generateHistogram::GHH300fWm370fWWm2220 ){ SampleName = "GHH300fWm370fWWm2220";}
if( m_process==generateHistogram::GHH300fWm960fWWm5760 ){ SampleName = "GHH300fWm960fWWm5760";}
if( m_process==generateHistogram::GHH300fW0fWWm4650 ){ SampleName = "GHH300fW0fWWm4650";}
if( m_process==generateHistogram::GHH300fW0fWWm2300 ){ SampleName = "GHH300fW0fWWm2300";}
if( m_process==generateHistogram::GHH300fW0fWWm7250 ){ SampleName = "GHH300fW0fWWm7250";}
if( m_process==generateHistogram::GHH300fW710fWWm4260 ){ SampleName = "GHH300fW710fWWm4260";}
if( m_process==generateHistogram::GHH300fW430fWWm2580 ){ SampleName = "GHH300fW430fWWm2580";}
if( m_process==generateHistogram::GHH300fW1110fWWm6660 ){ SampleName = "GHH300fW1110fWWm6660";}
if( m_process==generateHistogram::GHH300fW1000fWW2000 ){ SampleName = "GHH300fW1000fWW2000";}
if( m_process==generateHistogram::GHH300fW500fWW1000 ){ SampleName = "GHH300fW500fWW1000";}
if( m_process==generateHistogram::GHH300fW1500fWW3000 ){ SampleName = "GHH300fW1500fWW3000";}
if( m_process==generateHistogram::GHH300fW300fWW4500 ){ SampleName = "GHH300fW300fWW4500";}
if( m_process==generateHistogram::GHH300fW150fWW2250 ){ SampleName = "GHH300fW150fWW2250";}
if( m_process==generateHistogram::GHH300fW470fWW7050 ){ SampleName = "GHH300fW470fWW7050";}
if( m_process==generateHistogram::GHH300fWm330fWW4950 ){ SampleName = "GHH300fWm330fWW4950";}
if( m_process==generateHistogram::GHH300fWm150fWW2250 ){ SampleName = "GHH300fWm150fWW2250";}
if( m_process==generateHistogram::GHH300fWm490fWW7350 ){ SampleName = "GHH300fWm490fWW7350";}
if( m_process==generateHistogram::GHH300fWm1200fWW2400 ){ SampleName = "GHH300fWm1200fWW2400";}
if( m_process==generateHistogram::GHH300fWm500fWW1000 ){ SampleName = "GHH300fWm500fWW1000";}
if( m_process==generateHistogram::GHH300fWm1950fWW3900 ){ SampleName = "GHH300fWm1950fWW3900";}
if( m_process==generateHistogram::GHH300fWm1100fWWm2200 ){ SampleName = "GHH300fWm1100fWWm2200";}
if( m_process==generateHistogram::GHH300fWm500fWWm1000 ){ SampleName = "GHH300fWm500fWWm1000";}
if( m_process==generateHistogram::GHH300fWm1800fWWm3600 ){ SampleName = "GHH300fWm1800fWWm3600";}
if( m_process==generateHistogram::GHH300fWm300fWWm4500 ){ SampleName = "GHH300fWm300fWWm4500";}
if( m_process==generateHistogram::GHH300fWm140fWWm2100 ){ SampleName = "GHH300fWm140fWWm2100";}
if( m_process==generateHistogram::GHH300fWm470fWWm7050 ){ SampleName = "GHH300fWm470fWWm7050";}
if( m_process==generateHistogram::GHH300fW330fWWm4950 ){ SampleName = "GHH300fW330fWWm4950";}
if( m_process==generateHistogram::GHH300fW150fWWm2250 ){ SampleName = "GHH300fW150fWWm2250";}
if( m_process==generateHistogram::GHH300fW500fWWm7500 ){ SampleName = "GHH300fW500fWWm7500";}
if( m_process==generateHistogram::GHH300fW1100fWWm2200 ){ SampleName = "GHH300fW1100fWWm2200";}
if( m_process==generateHistogram::GHH300fW500fWWm1000 ){ SampleName = "GHH300fW500fWWm1000";}
if( m_process==generateHistogram::GHH300fW1800fWWm3600 ){ SampleName = "GHH300fW1800fWWm3600";}
if( m_process==generateHistogram::GHH600fW1350fWW0 ){ SampleName = "GHH600fW1350fWW0";}
if( m_process==generateHistogram::GHH600fW820fWW0 ){ SampleName = "GHH600fW820fWW0";}
if( m_process==generateHistogram::GHH600fW2130fWW0 ){ SampleName = "GHH600fW2130fWW0";}
if( m_process==generateHistogram::GHH600fW765fWW4590 ){ SampleName = "GHH600fW765fWW4590";}
if( m_process==generateHistogram::GHH600fW470fWW2820 ){ SampleName = "GHH600fW470fWW2820";}
if( m_process==generateHistogram::GHH600fW1205fWW7230 ){ SampleName = "GHH600fW1205fWW7230";}
if( m_process==generateHistogram::GHH600fW0fWW6200 ){ SampleName = "GHH600fW0fWW6200";}
if( m_process==generateHistogram::GHH600fW0fWW3800 ){ SampleName = "GHH600fW0fWW3800";}
if( m_process==generateHistogram::GHH600fW0fWW9750 ){ SampleName = "GHH600fW0fWW9750";}
if( m_process==generateHistogram::GHH600fWm855fWW5130 ){ SampleName = "GHH600fWm855fWW5130";}
if( m_process==generateHistogram::GHH600fWm520fWW3120 ){ SampleName = "GHH600fWm520fWW3120";}
if( m_process==generateHistogram::GHH600fWm1355fWW8130 ){ SampleName = "GHH600fWm1355fWW8130";}
if( m_process==generateHistogram::GHH600fWm1340fWW0 ){ SampleName = "GHH600fWm1340fWW0";}
if( m_process==generateHistogram::GHH600fWm790fWW0 ){ SampleName = "GHH600fWm790fWW0";}
if( m_process==generateHistogram::GHH600fWm2130fWW0 ){ SampleName = "GHH600fWm2130fWW0";}
if( m_process==generateHistogram::GHH600fWm755fWWm4530 ){ SampleName = "GHH600fWm755fWWm4530";}
if( m_process==generateHistogram::GHH600fWm455fWWm2730 ){ SampleName = "GHH600fWm455fWWm2730";}
if( m_process==generateHistogram::GHH600fWm1195fWWm7170 ){ SampleName = "GHH600fWm1195fWWm7170";}
if( m_process==generateHistogram::GHH600fW0fWWm6250 ){ SampleName = "GHH600fW0fWWm6250";}
if( m_process==generateHistogram::GHH600fW0fWWm3800 ){ SampleName = "GHH600fW0fWWm3800";}
if( m_process==generateHistogram::GHH600fW0fWWm9900 ){ SampleName = "GHH600fW0fWWm9900";}
if( m_process==generateHistogram::GHH600fW915fWWm5490 ){ SampleName = "GHH600fW915fWWm5490";}
if( m_process==generateHistogram::GHH600fW555fWWm3330 ){ SampleName = "GHH600fW555fWWm3330";}
if( m_process==generateHistogram::GHH600fW1440fWWm8640 ){ SampleName = "GHH600fW1440fWWm8640";}
if( m_process==generateHistogram::GHH600fW1200fWW2400 ){ SampleName = "GHH600fW1200fWW2400";}
if( m_process==generateHistogram::GHH600fW500fWW1000 ){ SampleName = "GHH600fW500fWW1000";}
if( m_process==generateHistogram::GHH600fW1900fWW3800 ){ SampleName = "GHH600fW1900fWW3800";}
if( m_process==generateHistogram::GHH600fW400fWW6000 ){ SampleName = "GHH600fW400fWW6000";}
if( m_process==generateHistogram::GHH600fW200fWW3000 ){ SampleName = "GHH600fW200fWW3000";}
if( m_process==generateHistogram::GHH600fW650fWW9750 ){ SampleName = "GHH600fW650fWW9750";}
if( m_process==generateHistogram::GHH600fWm400fWW6000 ){ SampleName = "GHH600fWm400fWW6000";}
if( m_process==generateHistogram::GHH600fWm200fWW3000 ){ SampleName = "GHH600fWm200fWW3000";}
if( m_process==generateHistogram::GHH600fWm650fWW9750 ){ SampleName = "GHH600fWm650fWW9750";}
if( m_process==generateHistogram::GHH600fWm1250fWW2500 ){ SampleName = "GHH600fWm1250fWW2500";}
if( m_process==generateHistogram::GHH600fWm500fWW1000 ){ SampleName = "GHH600fWm500fWW1000";}
if( m_process==generateHistogram::GHH600fWm2000fWW4000 ){ SampleName = "GHH600fWm2000fWW4000";}
if( m_process==generateHistogram::GHH600fWm1200fWWm2400 ){ SampleName = "GHH600fWm1200fWWm2400";}
if( m_process==generateHistogram::GHH600fWm500fWWm1000 ){ SampleName = "GHH600fWm500fWWm1000";}
if( m_process==generateHistogram::GHH600fWm1900fWWm3800 ){ SampleName = "GHH600fWm1900fWWm3800";}
if( m_process==generateHistogram::GHH600fWm400fWWm6000 ){ SampleName = "GHH600fWm400fWWm6000";}
if( m_process==generateHistogram::GHH600fWm190fWWm2850 ){ SampleName = "GHH600fWm190fWWm2850";}
if( m_process==generateHistogram::GHH600fWm650fWWm9750 ){ SampleName = "GHH600fWm650fWWm9750";}
if( m_process==generateHistogram::GHH600fW450fWWm6750 ){ SampleName = "GHH600fW450fWWm6750";}
if( m_process==generateHistogram::GHH600fW200fWWm3000 ){ SampleName = "GHH600fW200fWWm3000";}
if( m_process==generateHistogram::GHH600fW670fWWm10050 ){ SampleName = "GHH600fW670fWWm10050";}
if( m_process==generateHistogram::GHH600fW1300fWWm2600 ){ SampleName = "GHH600fW1300fWWm2600";}
if( m_process==generateHistogram::GHH600fW600fWWm1200 ){ SampleName = "GHH600fW600fWWm1200";}
if( m_process==generateHistogram::GHH600fW2050fWWm4100 ){ SampleName = "GHH600fW2050fWWm4100";}
if( m_process==generateHistogram::GHH900fW1570fWW0 ){ SampleName = "GHH900fW1570fWW0";}
if( m_process==generateHistogram::GHH900fW940fWW0 ){ SampleName = "GHH900fW940fWW0";}
if( m_process==generateHistogram::GHH900fW2510fWW0 ){ SampleName = "GHH900fW2510fWW0";}
if( m_process==generateHistogram::GHH900fW1015fWW6090 ){ SampleName = "GHH900fW1015fWW6090";}
if( m_process==generateHistogram::GHH900fW610fWW3660 ){ SampleName = "GHH900fW610fWW3660";}
if( m_process==generateHistogram::GHH900fW1615fWW9690 ){ SampleName = "GHH900fW1615fWW9690";}
if( m_process==generateHistogram::GHH900fW0fWW8800 ){ SampleName = "GHH900fW0fWW8800";}
if( m_process==generateHistogram::GHH900fW0fWW5250 ){ SampleName = "GHH900fW0fWW5250";}
if( m_process==generateHistogram::GHH900fW0fWW14100 ){ SampleName = "GHH900fW0fWW14100";}
if( m_process==generateHistogram::GHH900fWm1165fWW6990 ){ SampleName = "GHH900fWm1165fWW6990";}
if( m_process==generateHistogram::GHH900fWm695fWW4170 ){ SampleName = "GHH900fWm695fWW4170";}
if( m_process==generateHistogram::GHH900fWm1865fWW11190 ){ SampleName = "GHH900fWm1865fWW11190";}
if( m_process==generateHistogram::GHH900fWm1550fWW0 ){ SampleName = "GHH900fWm1550fWW0";}
if( m_process==generateHistogram::GHH900fWm920fWW0 ){ SampleName = "GHH900fWm920fWW0";}
if( m_process==generateHistogram::GHH900fWm2480fWW0 ){ SampleName = "GHH900fWm2480fWW0";}
if( m_process==generateHistogram::GHH900fWm1000fWWm6000 ){ SampleName = "GHH900fWm1000fWWm6000";}
if( m_process==generateHistogram::GHH900fWm495fWWm2970 ){ SampleName = "GHH900fWm495fWWm2970";}
if( m_process==generateHistogram::GHH900fWm1605fWWm9630 ){ SampleName = "GHH900fWm1605fWWm9630";}
if( m_process==generateHistogram::GHH900fW0fWWm8700 ){ SampleName = "GHH900fW0fWWm8700";}
if( m_process==generateHistogram::GHH900fW0fWWm5150 ){ SampleName = "GHH900fW0fWWm5150";}
if( m_process==generateHistogram::GHH900fW0fWWm13950 ){ SampleName = "GHH900fW0fWWm13950";}
if( m_process==generateHistogram::GHH900fW1205fWWm7230 ){ SampleName = "GHH900fW1205fWWm7230";}
if( m_process==generateHistogram::GHH900fW720fWWm4320 ){ SampleName = "GHH900fW720fWWm4320";}
if( m_process==generateHistogram::GHH900fW1925fWWm11550 ){ SampleName = "GHH900fW1925fWWm11550";}
if( m_process==generateHistogram::GHH900fW1400fWW2800 ){ SampleName = "GHH900fW1400fWW2800";}
if( m_process==generateHistogram::GHH900fW700fWW1400 ){ SampleName = "GHH900fW700fWW1400";}
if( m_process==generateHistogram::GHH900fW2300fWW4600 ){ SampleName = "GHH900fW2300fWW4600";}
if( m_process==generateHistogram::GHH900fW550fWW8250 ){ SampleName = "GHH900fW550fWW8250";}
if( m_process==generateHistogram::GHH900fW300fWW4500 ){ SampleName = "GHH900fW300fWW4500";}
if( m_process==generateHistogram::GHH900fW900fWW13500 ){ SampleName = "GHH900fW900fWW13500";}
if( m_process==generateHistogram::GHH900fWm600fWW9000 ){ SampleName = "GHH900fWm600fWW9000";}
if( m_process==generateHistogram::GHH900fWm300fWW4500 ){ SampleName = "GHH900fWm300fWW4500";}
if( m_process==generateHistogram::GHH900fWm1000fWW15000 ){ SampleName = "GHH900fWm1000fWW15000";}
if( m_process==generateHistogram::GHH900fWm1500fWW3000 ){ SampleName = "GHH900fWm1500fWW3000";}
if( m_process==generateHistogram::GHH900fWm800fWW1600 ){ SampleName = "GHH900fWm800fWW1600";}
if( m_process==generateHistogram::GHH900fWm2400fWW4800 ){ SampleName = "GHH900fWm2400fWW4800";}
if( m_process==generateHistogram::GHH900fWm1400fWWm2800 ){ SampleName = "GHH900fWm1400fWWm2800";}
if( m_process==generateHistogram::GHH900fWm700fWWm1400 ){ SampleName = "GHH900fWm700fWWm1400";}
if( m_process==generateHistogram::GHH900fWm2300fWWm4600 ){ SampleName = "GHH900fWm2300fWWm4600";}
if( m_process==generateHistogram::GHH900fWm550fWWm8250 ){ SampleName = "GHH900fWm550fWWm8250";}
if( m_process==generateHistogram::GHH900fWm250fWWm3750 ){ SampleName = "GHH900fWm250fWWm3750";}
if( m_process==generateHistogram::GHH900fWm850fWWm12750 ){ SampleName = "GHH900fWm850fWWm12750";}
if( m_process==generateHistogram::GHH900fW600fWWm9000 ){ SampleName = "GHH900fW600fWWm9000";}
if( m_process==generateHistogram::GHH900fW300fWWm4500 ){ SampleName = "GHH900fW300fWWm4500";}
if( m_process==generateHistogram::GHH900fW1000fWWm15000 ){ SampleName = "GHH900fW1000fWWm15000";}
if( m_process==generateHistogram::GHH900fW1500fWWm3000 ){ SampleName = "GHH900fW1500fWWm3000";}
if( m_process==generateHistogram::GHH900fW800fWWm1600 ){ SampleName = "GHH900fW800fWWm1600";}
if( m_process==generateHistogram::GHH900fW2400fWWm4800 ){ SampleName = "GHH900fW2400fWWm4800";}
if( m_process==generateHistogram::GHH300fW1350fWW0 ){ SampleName = "GHH300fW1350fWW0";}
if( m_process==generateHistogram::GHH360fW1350fWW0 ){ SampleName = "GHH360fW1350fWW0";}
if( m_process==generateHistogram::GHH420fW1350fWW0 ){ SampleName = "GHH420fW1350fWW0";}
if( m_process==generateHistogram::GHH480fW1350fWW0 ){ SampleName = "GHH480fW1350fWW0";}
if( m_process==generateHistogram::GHH540fW1350fWW0 ){ SampleName = "GHH540fW1350fWW0";}
if( m_process==generateHistogram::GHH660fW1350fWW0 ){ SampleName = "GHH660fW1350fWW0";}
if( m_process==generateHistogram::GHH720fW1350fWW0 ){ SampleName = "GHH720fW1350fWW0";}
if( m_process==generateHistogram::GHH780fW1350fWW0 ){ SampleName = "GHH780fW1350fWW0";}
if( m_process==generateHistogram::GHH840fW1350fWW0 ){ SampleName = "GHH840fW1350fWW0";}
if( m_process==generateHistogram::GHH900fW1350fWW0 ){ SampleName = "GHH900fW1350fWW0";}
if( m_process==generateHistogram::GHH300fW0fWW6200 ){ SampleName = "GHH300fW0fWW6200";}
if( m_process==generateHistogram::GHH360fW0fWW6200 ){ SampleName = "GHH360fW0fWW6200";}
if( m_process==generateHistogram::GHH420fW0fWW6200 ){ SampleName = "GHH420fW0fWW6200";}
if( m_process==generateHistogram::GHH480fW0fWW6200 ){ SampleName = "GHH480fW0fWW6200";}
if( m_process==generateHistogram::GHH540fW0fWW6200 ){ SampleName = "GHH540fW0fWW6200";}
if( m_process==generateHistogram::GHH660fW0fWW6200 ){ SampleName = "GHH660fW0fWW6200";}
if( m_process==generateHistogram::GHH720fW0fWW6200 ){ SampleName = "GHH720fW0fWW6200";}
if( m_process==generateHistogram::GHH780fW0fWW6200 ){ SampleName = "GHH780fW0fWW6200";}
if( m_process==generateHistogram::GHH840fW0fWW6200 ){ SampleName = "GHH840fW0fWW6200";}
if( m_process==generateHistogram::GHH900fW0fWW6200 ){ SampleName = "GHH900fW0fWW6200";}
if( m_process==generateHistogram::GHH1000fW1350fWW0 ){ SampleName = "GHH1000fW1350fWW0";}
if( m_process==generateHistogram::GHH1200fW1350fWW0 ){ SampleName = "GHH1200fW1350fWW0";}
if( m_process==generateHistogram::GHH1500fW1350fWW0 ){ SampleName = "GHH1500fW1350fWW0";}
if( m_process==generateHistogram::GHH2000fW1350fWW0 ){ SampleName = "GHH2000fW1350fWW0";}
if( m_process==generateHistogram::GHH1000fW0fWW6200 ){ SampleName = "GHH1000fW0fWW6200";}
if( m_process==generateHistogram::GHH1200fW0fWW6200 ){ SampleName = "GHH1200fW0fWW6200";}
if( m_process==generateHistogram::GHH1500fW0fWW6200 ){ SampleName = "GHH1500fW0fWW6200";}
if( m_process==generateHistogram::GHH2000fW0fWW6200 ){ SampleName = "GHH2000fW0fWW6200";}

  TString systree="Nom_";
  if(m_sysTree.Contains("Nominal")) {
    if(SampleName=="NonPrompt" && (m_description.Contains("ElNom")||m_description.Contains("MuNom")) ) systree = "";
    else systree = "Nom_";}
  else {
   systree = m_sysTree+"_"; //Greg!! 
  }
  if(SampleName=="data") systree = "_"; 
  histogramName += SampleName+systree; //Greg!!
  /*  
  if( m_ntags == 0 ) histogramName += "0tag";
  else if( m_ntags == 1 ) histogramName += "1tag";
  else if( m_ntags == 2 ) histogramName += "2tag";
  else if( m_ntags >= 3 ) histogramName += "3ptag";
  else if( m_ntags < 0 ) histogramName += "0ptag";
  
  if( m_njets == 1 ) histogramName += "1jet";
  else if( m_njets == 2 ) histogramName += "2jet";
  else if( m_njets == 3 ) histogramName += "3jet";
  else if( m_njets == 4 ) histogramName += "4jet";
  else if( m_njets >  4 ) histogramName += "5pjet";
  else if( m_njets < 0 ) histogramName += "0pjet";
  
  histogramName += "_";
  */
  /*
  if( m_nfjets == 0 ) histogramName += "0fjet";
  else if( m_nfjets == 1 ) histogramName += "1fjet";
  else if( m_nfjets == 2 ) histogramName += "2fjet";
  else if( m_nfjets == 3 ) histogramName += "3fjet";
  else if( m_nfjets > 3 ) histogramName += "4pfjet";
  else if( m_nfjets < 0 ) histogramName += "0pfjet";
  histogramName += "_";
  */
 /*
 if(!isCBA) {
  if( m_ptv > 0 && m_ptv < 75 ) histogramName += "0_75ptv";//Yanhui
  else if( m_ptv >= 75 &&m_ptv < 150) histogramName += "75_150ptv";//Yanhui
  else if( m_ptv >= 150 ) histogramName += "150ptv";
  else if( m_ptv < 0 ) histogramName += "0ptv";
 }
 else if (isCBA){
  if( m_ptv > 0 && m_ptv < 75 ) histogramName += "0_75ptv";//Yanhui
  else if( m_ptv >= 75 &&m_ptv < 150) histogramName += "75_150ptv";//Yanhui
  else if( m_ptv >= 150 &&m_ptv < 200) histogramName += "150_200ptv"; 
  else if( m_ptv >= 200) histogramName += "200ptv";
 }

  histogramName += "_";
  
  if(isCBA) m_description = "SR";
  */
  histogramName += m_description;

if(varName == "Norm") histogramName += "Norm";
else {
 histogramName += "_obs_";
 histogramName += varName;
}  

//histogramName += "_";

  //histogramName += variation;


  histogramName.ReplaceAll(" ","");

  return histogramName;

}
