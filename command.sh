path=/home/storage/Users/xuyue/atlas/GHH/GHH_AnalysisCode/outputs/AddFlatWtaggerSF/SR
pathSig=/home/storage/Users/xuyue/atlas/GHH/GHH_AnalysisCode/outputs/AddFlatWtaggerSF/SR/SignalNLO

./NtupleToHist 0 $path #data15
./NtupleToHist 1 $path #data16
./NtupleToHist 2 $path #data17
./NtupleToHist 3 $path #data18
./NtupleToHist 4 $pathSig #GHH600X
./NtupleToHist 5 $path #ttbar
./NtupleToHist 6 $path #singletopt
./NtupleToHist 7 $path #singletops
./NtupleToHist 8 $path #singletopWt
./NtupleToHist 9 $path #Wjets
./NtupleToHist 10 $path #Zjets
./NtupleToHist 11 $path #WWW
./NtupleToHist 12 $path #VVV
./NtupleToHist 13 $path #Wgamma
./NtupleToHist 14 $path #Zgamma
./NtupleToHist 15 $path #ssWW
./NtupleToHist 16 $path #ttW
./NtupleToHist 17 $path #ttZ
./NtupleToHist 18 $path #tZ
./NtupleToHist 19 $path #WZ
./NtupleToHist 20 $path #ZZ
./NtupleToHist 21 $pathSig #GHH300X
./NtupleToHist 22 $pathSig #GHH300Y
./NtupleToHist 23 $pathSig #GHH600Y
./NtupleToHist 24 $pathSig #GHH900X
#second signal
./NtupleToHist 25 $pathSig #GHH3f600f0
./NtupleToHist 26 $pathSig #GHH3fm600f0
./NtupleToHist 27 $pathSig #GHH3f350f2100
./NtupleToHist 28 $pathSig #GHH3fm350f2100
./NtupleToHist 29 $pathSig #GHH3fm350fm2100
./NtupleToHist 30 $pathSig #GHH3f350fm2100
./NtupleToHist 31 $pathSig #GHH3f0f3000
./NtupleToHist 32 $pathSig #GHH3f0fm3000
./NtupleToHist 33 $pathSig #GHH6f650f0
./NtupleToHist 34 $pathSig #GHH6fm650f0
./NtupleToHist 35 $pathSig #GHH6f400f2400
./NtupleToHist 36 $pathSig #GHH6fm400f2400
./NtupleToHist 37 $pathSig #GHH6fm400fm2400
./NtupleToHist 38 $pathSig #GHH6f400fm2400
./NtupleToHist 39 $pathSig #GHH6f0f3500
./NtupleToHist 40 $pathSig #GHH6f0fm3500
./NtupleToHist 41 $pathSig #GHH9f800f0
./NtupleToHist 42 $pathSig #GHH9fm800f0
./NtupleToHist 43 $pathSig #GHH9f600f3600
./NtupleToHist 44 $pathSig #GHH9fm600f3600
./NtupleToHist 45 $pathSig #GHH9fm600fm3600
./NtupleToHist 46 $pathSig #GHH9f600fm3600
./NtupleToHist 47 $pathSig #GHH9f0f5000
./NtupleToHist 48 $pathSig #GHH9f0fm5000
./NtupleToHist 49 $pathSig # WZ mc16a sys
./NtupleToHist 50 $pathSig # WZ mc16d sys
./NtupleToHist 51 $pathSig # WZ mc16e sys
